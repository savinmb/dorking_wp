<?php
require_once("wp-load.php");
?>
<?php
    if($_POST['form_type']=='street') 
	{
	$fargs= array(
	'post__not_in' =>array(explode('|',$_POST['feat'])),
	'post_type' => array( 'shop', 'events', 'hotel'),
	'posts_per_page' => 9,
	'paged'=>$_POST['page_value'],
	'tax_query' => array(
	array(
		'taxonomy' => 'Street',
		'field' => 'slug',
		'terms' => $_POST['street_v'],
		),
	),
	);
	$loop = new WP_Query($fargs);
	if($loop->have_posts()) 
		{ 
		$i=$_POST['count_value']+1;
		while($loop->have_posts()) : $loop->the_post();
		if(($i%3) == 1) 
			{
            echo '<div class="row common-row1 level-list" >';
            }		
		?>
		<?php
				if(($i%9) == 0 && $loop->max_num_pages>$_POST['page_value']) { ?>
				<div class="col-12 col-sm-12 col-md-4 col-lg-4 grow cursor-pointer load-section mb-4" id="element_<?php echo $i; ?>" style="display:none;">
                    <div class="magic-box-height bkyellow-color">
                        <div class="image_container image_container_pad0">
                        <img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($loop->ID), 'thumbnail' ); ?>" class="img-center img-fluid img_height">
                        </div>
                        <div class=" bk-orange-common text-center box-head-padding word-wrap">
                            <p class="col  color-white magic-box-head-size"> <?php the_title();  ?> </p>
                        </div>
                    </div>
                </div>		
				<div class="col-12 col-sm-12 col-md-4 col-lg-4 cursor-pointer mb-4 load-more ld_more" id="loadelement_<?php echo $i; ?>">
                            <div class="magic-box-height bk-gray  view_more">
                                <div class=" text-center magic-box-text-middle box-head-padding word-wrap">
                                    <p class="col  color-white magic-box-head-size"> view more</p>
                                </div>
                                <div class="image_container img_view_more">
                                    <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/index/viewmore.png" class="img-center img-fluid">
                                </div>
                            </div>
                </div>	
				<?php }
				else {   ?>
				<div class="col-12 col-sm-12 col-md-4 col-lg-4 grow cursor-pointer load-section  mb-4" id="element_<?php echo $i; ?>">
                    <div class="magic-box-height bkyellow-color">
                        <div class="image_container image_container_pad0">
                        <img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($loop->ID), 'thumbnail' ); ?>" class="img-center img-fluid img_height">
                        </div>
                        <div class=" bk-orange-common text-center box-head-padding word-wrap">
                            <p class="col  color-white magic-box-head-size"> <?php the_title();  ?> </p>
                        </div>
                    </div>
                </div>	
		<?php	}
				?>
        <?php   
        if (($i % 3) == 0  || $loop->post_count==$i) 
			{
			echo '</div>';
			}			
			++$i;
			endwhile;
		 }
		wp_reset_query();
	}
else if($_POST['form_type']=='category')
	{
	$loop = new WP_Query( array ('post_type' => array( 'shop', 'events', 'hotel'),'cat' => $_POST['category_id'],'post__not_in'=>array(explode('|',$_POST['feat'])),'posts_per_page' => 9,'paged'=>$_POST['page_value']));
	if($loop->have_posts()) 
		{ 
		$i=$_POST['count_value']+1;
		while($loop->have_posts()) : $loop->the_post();
		if(($i%3) == 1) 
			{
            echo '<div class="row common-row1 level-list" >';
            }		
		?>
		<?php
				if(($i%9) == 0 && $loop->max_num_pages>$_POST['page_value']) { ?>
				<div class="col-12 col-sm-12 col-md-4 col-lg-4 grow cursor-pointer load-section mb-4" id="element_<?php echo $i; ?>" style="display:none;">
                    <div class="magic-box-height bkyellow-color">
                        <div class="image_container image_container_pad0">
                        <img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($loop->ID), 'thumbnail' ); ?>" class="img-center img-fluid img_height">
                        </div>
                        <div class=" bk-orange-common text-center box-head-padding word-wrap">
                            <p class="col  color-white magic-box-head-size"> <?php the_title();  ?> </p>
                        </div>
                    </div>
                </div>		
				<div class="col-12 col-sm-12 col-md-4 col-lg-4 cursor-pointer mb-4 load-more cld_more" id="loadelement_<?php echo $i; ?>">
                            <div class="magic-box-height bk-gray  view_more">
                                <div class=" text-center magic-box-text-middle box-head-padding word-wrap">
                                    <p class="col  color-white magic-box-head-size"> view more</p>
                                </div>
                                <div class="image_container img_view_more">
                                    <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/index/viewmore.png" class="img-center img-fluid">
                                </div>
                            </div>
                </div>	
				<?php }
				else {   ?>
				<div class="col-12 col-sm-12 col-md-4 col-lg-4 grow cursor-pointer load-section  mb-4" id="element_<?php echo $i; ?>">
                    <div class="magic-box-height bkyellow-color">
                        <div class="image_container image_container_pad0">
                        <img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($loop->ID), 'thumbnail' ); ?>" class="img-center img-fluid img_height">
                        </div>
                        <div class=" bk-orange-common text-center box-head-padding word-wrap">
                            <p class="col  color-white magic-box-head-size"> <?php the_title();  ?> </p>
                        </div>
                    </div>
                </div>	
		<?php	}
				?>
        <?php   
        if (($i % 3) == 0  || $loop->post_count==$i) 
			{
			echo '</div>';
			}			
			++$i;
			endwhile;
		 }
		wp_reset_query();
	}
else if($_POST['form_type']=='archieve')
	{
	$fargs= array(
			    'post_type' => $_POST['post_type'],
				'posts_per_page' => 9,
				'paged'=>$_POST['page_value']
			 );	
	$loop = new WP_Query($fargs);		 
	if($loop->have_posts()) 
		{ 
		$i=$_POST['count_value']+1;
		while($loop->have_posts()) : $loop->the_post();
		if(($i%3) == 1) 
			{
            echo '<div class="row common-row1 level-list" >';
            }		
		?>
		<?php
				if(($i%9) == 0 && $loop->max_num_pages>$_POST['page_value']) { ?>
				<div class="col-12 col-sm-12 col-md-4 col-lg-4 grow cursor-pointer load-section mb-4" id="element_<?php echo $i; ?>" style="display:none;">
                    <div class="magic-box-height bkyellow-color">
                        <div class="image_container image_container_pad0">
                        <img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($loop->ID), 'thumbnail' ); ?>" class="img-center img-fluid img_height">
                        </div>
                        <div class=" bk-orange-common text-center box-head-padding word-wrap">
                            <p class="col  color-white magic-box-head-size"> <?php the_title();  ?> </p>
                        </div>
                    </div>
                </div>		
				<div class="col-12 col-sm-12 col-md-4 col-lg-4 cursor-pointer mb-4 load-more ald_more" id="loadelement_<?php echo $i; ?>">
                            <div class="magic-box-height bk-gray  view_more">
                                <div class=" text-center magic-box-text-middle box-head-padding word-wrap">
                                    <p class="col  color-white magic-box-head-size"> view more</p>
                                </div>
                                <div class="image_container img_view_more">
                                    <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/index/viewmore.png" class="img-center img-fluid">
                                </div>
                            </div>
                </div>	
				<?php }
				else {   ?>
				<div class="col-12 col-sm-12 col-md-4 col-lg-4 grow cursor-pointer load-section  mb-4" id="element_<?php echo $i; ?>">
                    <div class="magic-box-height bkyellow-color">
                        <div class="image_container image_container_pad0">
                        <img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($loop->ID), 'thumbnail' ); ?>" class="img-center img-fluid img_height">
                        </div>
                        <div class=" bk-orange-common text-center box-head-padding word-wrap">
                            <p class="col  color-white magic-box-head-size"> <?php the_title();  ?> </p>
                        </div>
                    </div>
                </div>	
		<?php	}
				?>
        <?php   
        if (($i % 3) == 0  || $loop->post_count==$i) 
			{
			echo '</div>';
			}			
			++$i;
			endwhile;
		 }
		wp_reset_query();
	
	}
else if($_POST['form_type']=='bid')
	{
	$fargs= array(
			    'post_type' => $_POST['post_type'],
				'posts_per_page' => 9,
				'paged'=>$_POST['page_value']
			 );	
	$loop = new WP_Query($fargs);		 
	if($loop->have_posts()) 
		{ 
		$i=$_POST['count_value']+1;
		while($loop->have_posts()) : $loop->the_post();
		if(($i%3) == 1) 
			{
            echo '<div class="row common-row1 level-list" >';
            }		
		?>
		<?php
				if(($i%9) == 0 && $loop->max_num_pages>$_POST['page_value']) { ?>
				<div class="col-12 col-sm-12 col-md-4 col-lg-4 grow cursor-pointer load-section mb-4" id="element_<?php echo $i; ?>" style="display:none;">
                    <div class="magic-box-height bkyellow-color">
                        <div class="image_container image_container_pad0">
                        <img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($loop->ID), 'thumbnail' ); ?>" class="img-center img-fluid img_height">
                        </div>
                        <div class=" bk-orange-common text-center box-head-padding word-wrap">
                            <p class="col  color-white magic-box-head-size"> <?php the_title();  ?> </p>
                        </div>
                    </div>
                </div>		
				<div class="col-12 col-sm-12 col-md-4 col-lg-4 cursor-pointer mb-4 load-more bld_more" id="loadelement_<?php echo $i; ?>">
                            <div class="magic-box-height bk-gray  view_more">
                                <div class=" text-center magic-box-text-middle box-head-padding word-wrap">
                                    <p class="col  color-white magic-box-head-size"> view more</p>
                                </div>
                                <div class="image_container img_view_more">
                                    <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/index/viewmore.png" class="img-center img-fluid">
                                </div>
                            </div>
                </div>	
				<?php }
				else {   ?>
				<div class="col-12 col-sm-12 col-md-4 col-lg-4 grow cursor-pointer load-section  mb-4" id="element_<?php echo $i; ?>">
                    <div class="magic-box-height bkyellow-color">
                        <div class="image_container image_container_pad0">
                        <img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($loop->ID), 'thumbnail' ); ?>" class="img-center img-fluid img_height">
                        </div>
                        <div class=" bk-orange-common text-center box-head-padding word-wrap">
                            <p class="col  color-white magic-box-head-size"> <?php the_title();  ?> </p>
                        </div>
                    </div>
                </div>	
		<?php	}
				?>
        <?php   
        if (($i % 3) == 0  || $loop->post_count==$i) 
			{
			echo '</div>';
			}			
			++$i;
			endwhile;
		 }
		wp_reset_query();
	
	}	
else if($_POST['form_type']=='shop')
	{
	if($_POST['category']!='')
	$posts = new WP_Query(array('post_type' => 'shop','posts_per_page' => 9,'paged'=>$_POST['page_value1'],'s' =>$_POST['search_term'],'category__in' =>array(explode('|',$_POST['category']))));
	else
	$posts = new WP_Query(array('post_type' => 'shop','posts_per_page' => 9,'paged'=>$_POST['page_value1'],'s' =>$_POST['search_term']));	
	if($posts->have_posts()) 
		{ 
	
		$count=$_POST['count_value1']+1;
		while ( $posts->have_posts() ) : $posts->the_post();
		if(($count%3)==1) {
		echo '<div class="row common-row1 shopclass level-list">';	
		}	
		?>
		<?php
				if(($count%9) == 0 && $posts->max_num_pages>$_POST['page_value1']) { ?>
				<div class="col-12 col-sm-12 col-md-4 col-lg-4 grow cursor-pointer load-section1 mb-4" id="element1_<?php echo $count; ?>" style="display:none;">
                    <div class="magic-box-height bkyellow-color">
                        <div class="image_container image_container_pad0">
                        <img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($posts->ID), 'thumbnail' ); ?>" class="img-center img-fluid img_height">
                        </div>
                        <div class=" bk-orange-common text-center box-head-padding word-wrap">
                            <p class="col  color-white magic-box-head-size"> <?php the_title();  ?> </p>
                        </div>
                    </div>
                </div>		
				<div class="col-12 col-sm-12 col-md-4 col-lg-4 cursor-pointer mb-4 load-more ld_more1" id="loadelement1_<?php echo $count; ?>">
                            <div class="magic-box-height bk-gray view_more">
                                <div class=" text-center magic-box-text-middle box-head-padding word-wrap">
                                    <p class="col  color-white magic-box-head-size"> view more</p>
                                </div>
                                <div class="image_container img_view_more">
                                    <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/index/viewmore.png" class="img-center img-fluid">
                                </div>
                            </div>
                </div>	
					
				<?php }
				else {   ?>
				<div class="col-12 col-sm-12 col-md-4 col-lg-4 grow cursor-pointer load-section1 mb-4" id="element1_<?php echo $count; ?>">
                    <div class="magic-box-height bkyellow-color">
                        <div class="image_container image_container_pad0">
                        <img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($posts->ID), 'thumbnail' ); ?>" class="img-center img-fluid img_height">
                        </div>
                        <div class=" bk-orange-common text-center box-head-padding word-wrap">
                            <p class="col  color-white magic-box-head-size"> <?php the_title();  ?> </p>
                        </div>
                    </div>
                </div>	
				<?php	}
				?>
		<?php
		if (($count % 3) == 0 || $posts->post_count==$count) {
			  echo '</div>';
        }
		++$count;
			
		endwhile;
		}
	}
else if($_POST['form_type']=='event')
	{
	if($_POST['category']!='')
	$posts = new WP_Query(array('post_type' => 'events','posts_per_page' => 9,'paged'=>$_POST['page_value2'],'s' =>$_POST['search_term'],'category__in' =>array(explode('|',$_POST['category']))));
	else
	$posts = new WP_Query(array('post_type' => 'events','posts_per_page' => 9,'paged'=>$_POST['page_value2'],'s' =>$_POST['search_term']));	
	if($posts->have_posts()) 
		{ 
	
		$count=$_POST['count_value2']+1;
		while ( $posts->have_posts() ) : $posts->the_post();
		if(($count%3)==1) {
		echo '<div class="row common-row1 eventclass level-list">';	
		}	
		?>
		<?php
				if(($count%9) == 0 && $posts->max_num_pages>$_POST['page_value2']) { ?>
				<div class="col-12 col-sm-12 col-md-4 col-lg-4 grow cursor-pointer load-section1 mb-4" id="element2_<?php echo $count; ?>" style="display:none;">
                    <div class="magic-box-height bkyellow-color">
                        <div class="image_container image_container_pad0">
                        <img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($posts->ID), 'thumbnail' ); ?>" class="img-center img-fluid img_height">
                        </div>
                        <div class=" bk-orange-common text-center box-head-padding word-wrap">
                            <p class="col  color-white magic-box-head-size"> <?php the_title();  ?> </p>
                        </div>
                    </div>
                </div>		
				<div class="col-12 col-sm-12 col-md-4 col-lg-4 cursor-pointer mb-4 load-more ld_more2" id="loadelement2_<?php echo $count; ?>">
                            <div class="magic-box-height bk-gray view_more">
                                <div class=" text-center magic-box-text-middle box-head-padding word-wrap">
                                    <p class="col  color-white magic-box-head-size"> view more</p>
                                </div>
                                <div class="image_container img_view_more">
                                    <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/index/viewmore.png" class="img-center img-fluid">
                                </div>
                            </div>
                </div>	
					
				<?php }
				else {   ?>
				<div class="col-12 col-sm-12 col-md-4 col-lg-4 grow cursor-pointer load-section1 mb-4" id="element2_<?php echo $count; ?>">
                    <div class="magic-box-height bkyellow-color">
                        <div class="image_container image_container_pad0">
                        <img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($posts->ID), 'thumbnail' ); ?>" class="img-center img-fluid img_height">
                        </div>
                        <div class=" bk-orange-common text-center box-head-padding word-wrap">
                            <p class="col  color-white magic-box-head-size"> <?php the_title();  ?> </p>
                        </div>
                    </div>
                </div>	
				<?php	}
				?>
		<?php
		if (($count % 3) == 0 || $posts->post_count==$count) {
			  echo '</div>';
        }
		++$count;
			
		endwhile;
		}
	}
else if($_POST['form_type']=='hotel')
	{
	if($_POST['category']!='')
	$posts = new WP_Query(array('post_type' => 'hotel','posts_per_page' => 9,'paged'=>$_POST['page_value3'],'s' =>$_POST['search_term'],'category__in' =>array(explode('|',$_POST['category']))));
	else
	$posts = new WP_Query(array('post_type' => 'hotel','posts_per_page' => 9,'paged'=>$_POST['page_value3'],'s' =>$_POST['search_term']));	
	if($posts->have_posts()) 
		{ 
	
		$count=$_POST['count_value3']+1;
		while ( $posts->have_posts() ) : $posts->the_post();
		if(($count%3)==1) {
		echo '<div class="row common-row1 hotelclass level-list">';	
		}	
		?>
		<?php
				if(($count%9) == 0 && $posts->max_num_pages>$_POST['page_value3']) { ?>
				<div class="col-12 col-sm-12 col-md-4 col-lg-4 grow cursor-pointer load-section1 mb-4" id="element3_<?php echo $count; ?>" style="display:none;">
                    <div class="magic-box-height bkyellow-color">
                        <div class="image_container image_container_pad0">
                        <img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($posts->ID), 'thumbnail' ); ?>" class="img-center img-fluid img_height">
                        </div>
                        <div class=" bk-orange-common text-center box-head-padding word-wrap">
                            <p class="col  color-white magic-box-head-size"> <?php the_title();  ?> </p>
                        </div>
                    </div>
                </div>		
				<div class="col-12 col-sm-12 col-md-4 col-lg-4 cursor-pointer mb-4 load-more ld_more3" id="loadelement3_<?php echo $count; ?>">
                            <div class="magic-box-height bk-gray view_more">
                                <div class=" text-center magic-box-text-middle box-head-padding word-wrap">
                                    <p class="col  color-white magic-box-head-size"> view more</p>
                                </div>
                                <div class="image_container img_view_more">
                                    <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/index/viewmore.png" class="img-center img-fluid">
                                </div>
                            </div>
                </div>	
					
				<?php }
				else {   ?>
				<div class="col-12 col-sm-12 col-md-4 col-lg-4 grow cursor-pointer load-section1 mb-4" id="element3_<?php echo $count; ?>">
                    <div class="magic-box-height bkyellow-color">
                        <div class="image_container image_container_pad0">
                        <img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($posts->ID), 'thumbnail' ); ?>" class="img-center img-fluid img_height">
                        </div>
                        <div class=" bk-orange-common text-center box-head-padding word-wrap">
                            <p class="col  color-white magic-box-head-size"> <?php the_title();  ?> </p>
                        </div>
                    </div>
                </div>	
				<?php	}
				?>
		<?php
		if (($count % 3) == 0 || $posts->post_count==$count) {
			  echo '</div>';
        }
		++$count;
			
		endwhile;
		}
	}		
?>


