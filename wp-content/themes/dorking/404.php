<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>
<section id="loyalfree_banner">
    <div class="video-section-div">
		<div class="container-fluid pl-0 pr-0">
			<img src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/map_landing/map_landing_banner.png" class="w-100">
		</div>
		<div class="overlay_img_txt_loyal"><p class="overlay_img_txt_loyal_p font20 font_heavy"></p></div>
    </div>             

</section>
<section  class="breadcrumb_wrapper" >
                <div class="container my-3">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                          <li class="breadcrumb-item"><a href="<?php echo esc_url( home_url( '/' ) ); ?>">Home</a></li>
                          <li class="breadcrumb-item active" >404</li>
                        </ol>
                    </nav>
                </div>
</section>
<section  class="description">
    <div class="container">
				<header class="page-header">
					<h1 class="page-title"><?php _e( 'Oops! That page can&rsquo;t be found.', 'twentysixteen' ); ?></h1>
				</header><!-- .page-header -->

				<div class="page-content">
				<p><?php _e( 'It looks like nothing was found at this location. Maybe try a search?', 'twentysixteen' ); ?></p>
				 </div>
</section>

			
			

<?php get_footer(); ?>
