<?php
/**
* A Simple Category Template
*/
 
get_header(); ?> 
 
<section id="primary" class="site-content">
<div id="content" role="main">
<?php $args=array( 
'post_type' => 'shop', //set the post_type to use.
'posts_per_page' => 10  // how many posts or comment out for all.       
);

$items = new WP_Query($args);
if($items->have_posts()) : while($items->have_posts()) :


$items->the_post();

get_template_part( 'content' ); //or whatever method your theme uses for displaying content. 

endwhile; endif; //end the custom post_type loop

?>
</div>
</section>
 
 
<?php get_sidebar(); ?>
<?php get_footer(); ?>