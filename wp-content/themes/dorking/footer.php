<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>
 <footer>
                <div class="container">
                    <div class="row mb-4">
                        <div class="col-12 col-md-7">
                            <h6 class="foot_head mt-2 mb-4 d-none d-md-block"><?php echo get_field('main_menu_name',212) ?></h6>
                            <div class="row" style="    padding: 0px 0px 0px 15px;">
                                <div class="col-12 col-md-2 links_wrapper links_wrapper_mobview_style">
                                    <h6 class="footer_black footer_ul_head footer_links_ul_head d-block d-md-none" aria-expanded="false" data-toggle="collapse" data-target="#welcome_footer_expand">Welcome</h6>
                                    <h6 class="cyan font_heavy footer_links_ul_head d-none d-md-block"><?php echo get_field('sub_menu_1',212) ?></h6>
									<?php echo wp_nav_menu( array('menu' => 'Welcome','container'=>'','menu_class' => 'pl-0 collapse mt-3','menu_id'=>'welcome_footer_expand') ); ?>
                                </div>
                                <div class="col-12 col-md-2 links_wrapper links_wrapper_mobview_style">
                                    <h6 class="footer_black footer_ul_head footer_links_ul_head d-block d-md-none" aria-expanded="false"  data-toggle="collapse" data-target="#getting_her_footer_expand">Getting Here</h6>
                                    <h6 class="cyan font_heavy footer_links_ul_head d-none d-md-block"><?php echo get_field('sub_menu_2',212) ?></h6>
                                    <?php echo wp_nav_menu( array('menu' => 'Getting Here','container'=>'','menu_class' => 'pl-0 collapse mt-3','menu_id'=>'getting_her_footer_expand') ); ?>
								</div>
                                <div class="col-12 col-md-2 links_wrapper links_wrapper_mobview_style">  
                                    <h6 class="footer_black footer_ul_head footer_links_ul_head d-block d-md-none" aria-expanded="false"  data-toggle="collapse" data-target="#explore_footer_expand">Explore</h6>
                                    <h6 class="cyan font_heavy footer_links_ul_head d-none d-md-block"><?php echo get_field('sub_menu_3',212) ?></h6>
									<?php echo wp_nav_menu( array('menu' => 'Explore','container'=>'','menu_class' => 'pl-0 collapse mt-3','menu_id'=>'explore_footer_expand') ); ?>
                                </div>
                                <div class="col-12 col-md-2 links_wrapper links_wrapper_mobview_style">  
                                    <h6 class="footer_black footer_ul_head footer_links_ul_head d-block d-md-none mb-2" aria-expanded="false"  data-toggle="collapse" data-target="#shop_footer_expand">Shop</h6>
                                    <h6 class="cyan font_heavy footer_links_ul_head d-none d-md-block"><?php echo get_field('sub_menu_4',212) ?></h6>
									<?php echo wp_nav_menu( array('menu' => 'Shop','container'=>'','menu_class' => 'pl-0 collapse mt-3','menu_id'=>'shop_footer_expand') ); ?>
                                </div>                       
                                <div class="col-12 col-md-3 links_wrapper footer_dorking_download links_wrapper_mobview_style">
                                    <h6 class="footer_black footer_ul_head footer_links_ul_head d-block d-md-none" aria-expanded="false"  data-toggle="collapse" data-target="#dorking_footer_expand">Dorking Loyalfree</h6>
                                    <h6 class="cyan font_heavy footer_links_ul_head d-none d-md-block"><?php echo get_field('sub_menu_5',212) ?></h6>
                                    <ul class="pl-0 collapse mt-3" id="dorking_footer_expand">
                                      <li class="marbot"><a href="<?php echo get_permalink(49);  ?>"><img src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/footer_img_1.png"></a><li>
                                    </ul>    
                                </div>
                                <div class="col-12 col-md-12 links_wrapper links_wrapper_mobview_style">
                                    <h6 class="footer_black footer_ul_head footer_links_ul_head d-block d-md-none"  aria-expanded="false" data-toggle="collapse" data-target="#BID_footer_expand">BID Login</h6>
                                    <h6 class="cyan font_heavy footer_links_ul_head d-none d-md-block"><?php echo get_field('sub_menu_6',212) ?></h6>
                                        <?php 
                                        if(current_user_can('subscriber')) {
                                            echo wp_nav_menu( array('menu' => 'BID With Login','container'=>'','menu_class' => 'pl-0 collapse mt-3','menu_id'=>'BID_footer_expand') ); 
                                        } else {
                                            echo wp_nav_menu( array('menu' => 'BID Without Login','container'=>'','menu_class' => 'pl-0 collapse mt-3','menu_id'=>'BID_footer_expand') ); 
                                        }
                                        ?>
                                </div>
                                                                                         
                            </div>
                        </div>
                        <div class="col-12 col-md-5">
                           <div class="container">
                            <div class="row mt-3 mb-3 signup_condent">
                                <p class="font_heavy color4a"><?php echo get_field('footer_text',212) ?></p>
                            </div>
                            <?php  echo es_subbox( $namefield =  "NO", $desc = "", $group = "" );  ?> 
                               <div class="row mt-2">
                                   <div class="col-12">
                                       <div class="container">
                                           <div class="row">
                                               <div class="col-9 links_wrapper pl-0">
                                                   <h6 class="cyan font_heavy"><?php echo get_field('sub_menu_7',212) ?></h6>
                                                   <a class="pull-left " href="<?php echo get_field('instagram_link',212) ?>" target="_blank"><img class="img-fluid grow cursor-pointer" src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/social_1.png"/></a>
                                                   <a class="pull-left" href="<?php echo get_field('facebook_link',212) ?>" target="_blank"><img class="img-fluid grow cursor-pointer" src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/social_2.png"/></a>
                                                   <a class="pull-left" href="<?php echo get_field('twitter_link',212) ?>" target="_blank"><img class="img-fluid grow cursor-pointer" src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/social_3.png"/></a>
                                               </div>
                                               <div class="col-3 links_wrapper pl-0 pr-0">
                                                    <h6 class="cyan font_heavy"><?php echo get_field('sub_menu_8',212) ?></h6>
                                                    <a href="<?php echo get_field('trip_link',212) ?>" target="_blank"><img class="img-fluid grow cursor-pointer mtop16" src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/social_4.png"/></a>
                                               </div>
                                           </div>
                                       </div>
                                   </div>
                               </div>
                          </div>
                        </div>
                        
                    </div>
                    
                    <div class="row mb-4">
                         <div class="col-3">
						    <a href="<?php echo get_field('link_1',212) ?>" target="_blank">
                            <img class="img-fluid" src="<?php echo get_field('logo_1',212) ?>" />
							</a>
                        </div>
                        <div class="col-3">
						    <a href="<?php echo get_field('link_2',212) ?>"" target="_blank">
                            <img class="img-fluid " src="<?php echo get_field('logo_2',212) ?>" />
							</a>
                        </div>
                        <div class="col-3 text-center ">
						    <a href="<?php echo get_field('link_3',212) ?>"" target="_blank">
                            <img class="img-fluid" src="<?php echo get_field('logo_3',212) ?>" />
							</a>
                        </div>
                        <div class="col-3 text-center">
						    <a href="<?php echo get_field('link_4',212) ?>"" target="_blank">
                            <img class="img-fluid" src="<?php echo get_field('logo_4',212) ?>" />
							</a>
                        </div>
                    </div>
                    <div class="row ">
                        <div class="container pad0lg">
                            <p class="color4a"><?php echo get_field('copy_right_text',212) ?></p>
                        </div>
                    </div>

                </div>
            </footer>
			</main>
<script src="<?php bloginfo( 'stylesheet_directory' ); ?>/js/jquery-3.3.1.min.js"></script>
<script src="<?php bloginfo( 'stylesheet_directory' ); ?>/js/bootstrap.min.js"></script>
<script src="<?php bloginfo( 'stylesheet_directory' ); ?>/js/index_style.js"></script>	
<script src="<?php bloginfo( 'stylesheet_directory' ); ?>/js/pagination.js"></script>		
<?php wp_footer(); ?>
</body>
</html>
