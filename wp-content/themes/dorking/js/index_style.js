
$(document).ready(function () {
    $(".hovnext").mouseover(function () {
        $(this).parent().next().find(".display_img").removeClass("dispnone");
        $(this).addClass("dispnone");
    });
    $(".display_img").mouseout(function () {
        $(this).addClass("dispnone");
        $(this).parent().parent().prev().find(".hovnext").removeClass("dispnone");
    });
//   FOR MOBILE BURGER MEUNU AND SEARCH ICONS
    $("#search_show").click(function () {
        openSearch();
    });
    $("#search_wrapper .closebtn").click(function () {
        closeSearch();
    });
    $(document).on('click', '.close_nav', function () {
        closeNav();
    });
    $("#nav_toggl").click(function () {
        openNav();
    });
    $("#myCarousel").on("touchstart", function (event) {
        var xClick = event.originalEvent.touches[0].pageX;
        $(this).one("touchmove", function (event) {
            var xMove = event.originalEvent.touches[0].pageX;
            if (Math.floor(xClick - xMove) > 5) {
                $(this).carousel('next');
            } else if (Math.floor(xClick - xMove) < -5) {
                $(this).carousel('prev');
            }
        });
        $("#myCarousel").on("touchend", function () {
            $(this).off("touchmove");
        });
    });

    $(".dropdown-menu").click(function (e) {
        if (event.target.tagName.toLowerCase() !== 'i')
        {
            e.stopPropagation();
        }
    });

    window.onscroll = function () {
        myFunction();
        myFunctionmob();
        if ($(window).width() > 767) {
            resizeContent();
        }
    };
    /*disktop sticky header start*/

    var header = document.getElementById("navbarSupportedContent");
    var sticky = header.offsetTop - 55;


    function myFunction() {
        var flag = 0;
        $("li.mega-menu-item").each(function () {
            if ($(this).hasClass() != "mega-toggle-on") {
                flag == 1;
            }

            if (flag != 1) {
                if (window.pageYOffset > sticky) {
                    header.classList.add("sticky");
                } else {
                    header.classList.remove("sticky");
                }
            }
        });
    }


    jQuery(function ($) {

        $('li.mega-menu-item').on('open_panel', function () {
            $('body').addClass('overscroll');
            $(".close_new").css('display', 'block');
            $('li.mega-menu-item').removeClass("mega-toggle-on");
            $(this).addClass("mega-toggle-on");
        });

        $('li.mega-menu-item').on('close_panel', function () {
            $('body').removeClass('overscroll');
            $(".close_new").css('display', 'none');
        });

        $('.mega-menu-toggle').on('click', function () {
            if ($(this).hasClass('mega-menu-open')) {
                console.log("has class");
                $('body').addClass('overscroll');
                $("#search_show.fa.fa-search").addClass('search_icon_bar');
                $(".logo_mob").css('padding-left', '45px');
            } else {
                console.log("no class");
                $('body').removeClass('overscroll');
                $("#search_show.fa.fa-search").removeClass('search_icon_bar');
                $(".logo_mob").css('padding-left', '55px');
            }
        });

    });

    $('.links_wrapper').click(function () {
        $(this).siblings('.links_wrapper').children('.collapse').removeClass('show');
        $(this).siblings('.links_wrapper').children('.footer_ul_head').attr("aria-expanded", "false");
    });

    /*disktop sticky header end*/

    /*mobile sticky header  start*/

    var headermob = document.getElementById("navbarSupportedContentMob");
    var stickymob = header.offsetTop;


    function myFunctionmob() {
        var flag = 0;
        $(".mega-menu-toggle").each(function () {
            if ($(this).hasClass() != "mega-menu-open") {
                flag == 1;
            }

            if (flag != 1) {
                if (window.pageYOffset > stickymob) {
                    headermob.classList.add("sticky");
                } else {
                    headermob.classList.remove("sticky");
                }
            }
        });
    }

    /*mobile sticky header  end*/

    /*change by savin start*/

    $("#search-term").keypress(function (event) {
        if (event.which == 13) {
            event.preventDefault();
            $("#searchform").submit();
        }
    });
    $(".wp_autosearch_submit").click(function (event) {
        if ($('.wp_autosearch_input').val() == '')
        {
            return false;
        } else
        {
            window.location.href = $('.wp_autosearch_more').attr('href');
        }
    });
    $(".cat-search").click(function (e) {
        if ($(this).hasClass("disable-div"))
            return false;
        else
        {
            var ele = ($(this).attr('id')).split("_");
            var url = window.location.href + '&cat=' + ele[1];
            window.location.href = url;
        }

    });
    $(".cat_close").click(function (e) {
        var url = "";
        var cat_id = ($(this).attr('id')).split("_");
        var cval = $('#caturl_' + cat_id[1]).val();
        $("#catsr_" + cat_id[1]).hide();
        url = (window.location.href).replace('&' + cval, '');
        window.location.href = url;

    });





    $(".eventclass").slice(0, 3).css('display', 'flex');
    $(".ld_more2").on('click', function (e) {
        var eid = ($(this).attr('id')).split("_");
        $("#loadelement2_" + eid[1]).hide();
        $("#element2_" + eid[1]).show();
        $(".eventclass:hidden").slice(0, 3).css('display', 'flex').slideDown();
    });

    $(".hotelclass").slice(0, 3).css('display', 'flex');
    $(".ld_more3").on('click', function (e) {
        var eid = ($(this).attr('id')).split("_");
        $("#loadelement3_" + eid[1]).hide();
        $("#element3_" + eid[1]).show();
        $(".hotelclass:hidden").slice(0, 3).css('display', 'flex').slideDown();
    });

    /*change by savin end*/

    $(window).resize(function () {
        if ($(window).width() > 767) {
            resizeContent();
        }
    });

    msieversion();

});
function resizeContent() {
    var windowheight = $(window).height();
    if ($('#navbarSupportedContent').hasClass('sticky')) {
        var headerheight = $('#navbarSupportedContent').height();
        var remainheight = windowheight - headerheight;
        remainheight = remainheight + 30;
        $('li.mega-menu-row').parent().height(remainheight);
    } else {
        var headerheight = $('.header').height();
        var remainheight = windowheight - headerheight;
        remainheight = remainheight + 30;
        $("li.mega-menu-row").parent().height(remainheight);
    }
}
function openSearch() {
    $("#search_wrapper").css("width", "100%");
}
function closeSearch() {
    $("#search_wrapper").css("width", "0px");
}
function openNav() {
    $("#nav_wrapper").css("width", "100%");
}

function closeNav() {
    $("#mega-menu-wrap-primary #mega-menu-primary li.mega-menu-item").removeClass('mega-toggle-on');
    $('body').removeClass('overscroll');
}
function msieversion() {
    var ua = window.navigator.userAgent;
    var msie = ua.indexOf('MSIE ');
    if (msie > 0) {
        // IE 10 or older => return version number
        $("#e30_shape").css("stroke-width", "0.08px");
        $("#e31_shape").css("stroke-width", "0.08px");
        $("#e26_shape").css("stroke-width", "0.08px");
    }

    var trident = ua.indexOf('Trident/');
    if (trident > 0) {
        // IE 11 => return version number
        var rv = ua.indexOf('rv:');
        $("#e30_shape").css("stroke-width", "0.08px");
        $("#e31_shape").css("stroke-width", "0.08px");
        $("#e26_shape").css("stroke-width", "0.08px");
    }

    var edge = ua.indexOf('Edge/');
    if (edge > 0) {
        // Edge (IE 12+) => return version number
        $("#e30_shape").css("stroke-width", "0.08px");
        $("#e31_shape").css("stroke-width", "0.08px");
        $("#e26_shape").css("stroke-width", "0.08px");
    }
}





