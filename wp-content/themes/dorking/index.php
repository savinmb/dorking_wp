<?php
/*
    Template Name:Home
*/
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); 

while(have_posts())  : the_post(); 
?>

            <section id="video-section" class="" >
                <div class="video-section-div" style="height:500px;">
                    <div class="container-fluid pad0">
					<img src="<?php echo get_field('slider_image',get_the_ID()) ?>" class="w-100">
                        
                    </div>
                    <div class="overlay_img_txt"><p class="overlay_img_txt_p font_heavy"><?php echo get_field( 'text', get_the_ID() ); ?></p></div>
                </div>
            </section>
            <section id="social_icon_mob" class="social_icon_section d-block d-md-none" >
                <div class="container">
                    <div class="row">
                        <div class="col-7 col-sm-9">
                            <div class="row">
                                <div class="social_icons_head_mob">
                                    <h6 class="font14">Share :</h6>
                                </div>
                                <div class="social_icons_mob">
                                    <a href="<?php echo get_field('facebook_link',212) ?>" target="_blank"><img src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/index/Facebook.png"/></a>
                                    <a  href="<?php echo get_field('instagram_link',212) ?>" target="_blank"><img src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/index/Instagram.png"/></a>
                                    <a  href="<?php echo get_field('facebook_link',212) ?>" target="_blank"><img src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/index/Twitter.png"/></a>
                                </div>
                            </div>
                        </div>
                        <!--<div class="col-4"></div>-->
                        <div class="col-5 col-sm-3">
                            <div class="row">
                                <div class="download_head_mob ">
                                    <h6 class="font14">Download :</h6>
                                </div>
                                <div class="download__icons_mob">
                                    <a class="" href=""><img src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/index/loyalfreelogo.png" class="img-fluid"/></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section id="below-vid-div" class="section-padding " >
                <div class="container">
                 <?php 
                
                 the_content(); 
                
                 ?>
                </div> 
            </section>
			<section id="magic-boxes" class="section-padding d-none d-md-block">
                <div class="container">
				    <?php 
					
					$i=1;
                    $j=1;
					$rows = get_field('tiles',get_the_ID());
					
                    foreach($rows as $row) {
					if(($i%3) == 1) {
                                  echo '<div class="row common-row'.$j.'" >';
                    }
					if($row['image'] == '' && $row['name']!='Instagram feed'){
                        ?>
                            
                            <div class="col-12 col-sm-12 col-md-4 col-lg-4 img_bulg">
							    <a href="<?php echo $row['link']; ?>">
                                <div class="magic-box-height <?php echo $row['color'];  ?>">
                                    <div class="text-center magic-box-text-middle word-wrap">
                                        <p><?php echo $row['name']; ?></p>
                                    </div>
                                </div>
								</a>
                            </div>
                             <?php } 
							 else if($row['name']=='Instagram feed') { ?>
							  <div class="col-12 col-sm-12 col-md-4 col-lg-4 img_bulg"> 
							  <div class=" magic-box-height">
							    <?php echo do_shortcode('[instagram-feed imageres=medium num=1 cols=1 showheader=false showfollow=false showbutton=false imagepadding=0]');  ?>
							  </div>
							   </div>
							 <?php } else { ?>
                            <div class="col-12 col-sm-12 col-md-4 col-lg-4 img_bulg">
							    <a href="<?php echo $row['link']; ?>">
                                <div class=" magic-box-height <?php echo $row['color'];  ?>">
                                    <div class="image_container">
                                        <img src="<?php echo $row['image']; ?>" class="img-center img-fluid">
                                    </div>
                                    <div class=" bk-orange-common text-center box-head-padding">
                                        <p class="col color-white magic-box-head-size"> <?php echo $row['name']; ?> </p>
                                    </div>
                                </div>
								</a>
                            </div>
                <?php } 
				if (($i % 3) == 0 || sizeof($rows)==$i) {
					echo '</div>';
					$j++;
					}
				$i++;
				
				
				}  ?>
                </div> 
            </section>
			<section id="magic-boxes" class="section-padding-carousel d-block d-md-none"> 
                <div id="myCarousel" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
					<?php for($i=0;$i<sizeof($rows);$i++) {  ?>
                        <li data-target="#myCarousel" data-slide-to="<?php echo $i; ?>" <?php if($i==0) {  ?>class="active" <?php } ?>></li>
                    <?php } ?>	
                    </ol>

                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">
					    <?php 
						$c=0;
						foreach($rows as $row) {
						if($row['image'] == ''){
                        ?>
                        <div class="carousel-item <?php if($c==0) {  echo 'active';  }  ?>">
                            <div class="col-12 col-sm-12 col-md-4 col-lg-4 ">
							     <a href="<?php echo $row['link']; ?>">
                                <div class="magic-box-height <?php echo $row['color'];  ?>">
                                    <div class="text-center text-middle magic-box-text-middle word-wrap font14">
                                        <p><?php echo $row['name']; ?></p>
                                    </div>
                                </div>
								</a>
                            </div>
                        </div>
						<?php } else { ?>
                        <div class="carousel-item <?php if($c==0) {  echo 'active';  }  ?>">
                            <div class="col-12 col-sm-12 col-md-4 col-lg-4 ">
							<a href="<?php echo $row['link']; ?>">
                                <div class=" magic-box-height <?php echo $row['color'];  ?>">
                                    <div class="image_container">
                                        <img src="<?php echo $row['image']; ?>" class="img-center img-fluid">
                                    </div>
                                    <div class=" bk-orange-common text-center box-head-padding">
                                        <p class="col color-white font14"> <?php echo $row['name']; ?> </p>
                                    </div>
                                </div>
							</a>	
                            </div>
                        </div>
						<?php 
						          }
						++$c;
						} ?>
                    </div>

                </div>
            </section>
			
			
         
            <section id="explore_dorking_content" class="section-padding">
                <div class="container">
                    <div class="paddingbtm10 margintop25">
                        <h1 class="text-center color4a explore_dorking_head"><?php echo get_field( 'title', get_the_ID() ); ?> </h1>
                    </div>                   
                    <div class=" paddingbtm10">
                        <p class="text-justify blw-explore-content-p color4a"><?php echo get_field( 'description', get_the_ID() ); ?> 
                        </p>
                    </div>
                </div> 
            </section>
            <section id="map-img-scontent" class="mt-5">
                 <?php include 'svgfile.php';   ?>
            </section>
                    <section id="transport_content" class="section-padding mtop_main ">
                <div class="container pad0">
                    <div class=" paddingbtm10 margintop25 mb-1">
                        <h1 class="text-center color4a transport_content_head"><?php echo get_field( 'getting_text', get_the_ID() ); ?></h1>
                    </div>                   
                    <div class="row mt-4 transport-row">
                        <div class="col-sm-6 col-6 col-md-4 col-lg-4  mb-4">
                            <div class="transport_columns road_column">
                                <div class="by_road_img">
                                     <a href="<?php echo get_field( 'link1', get_the_ID() ); ?>"><img class="img-fluid transport-columns-w-100"  src="<?php echo get_field( 'image1', get_the_ID() ); ?> " alt="road"></a>
                                </div>                            
                                <div class=" text-center mt-4">
                                    <h3 class="transport_below_heading"><?php echo get_field( 'text1', get_the_ID() ); ?> </h3>
                                </div>
                            </div>    

                        </div><!-- /.col-lg-4 -->
                        <div class="col-sm-6 col-6 col-md-4 col-lg-4  mb-4 public_transport_column">
                            <div class=" transport_columns public_transport_column">
                                <div class="by_public_transport_img">
                                    <a href="<?php echo get_field( 'link2', get_the_ID() ); ?>"><img class="img-fluid transport-columns-w-100" src="<?php echo get_field( 'image2', get_the_ID() ); ?> " alt="public"></a>
                                </div>
                                <div class="  text-center mt-4">
                                    <h3 class="transport_below_heading"><?php echo get_field( 'text2', get_the_ID() ); ?> </h3>
                                </div>
                            </div>

                        </div><!-- /.col-lg-4 -->
                        <div class="col-sm-6 col-6 col-md-4 col-lg-4  mb-4 footpath_column ">
                            <div class="transport_columns footpath_column">
                                <div class="by_footpath_img">
                                     <a href="<?php echo get_field( 'link3', get_the_ID() ); ?>"><img class="img-fluid transport-columns-w-100" src="<?php echo get_field( 'image3', get_the_ID() ); ?> " alt="footpath" ></a>
                                </div>
                                <div class="  text-center mt-4">
                                    <h3 class="transport_below_heading"><?php echo get_field( 'text3', get_the_ID() ); ?> </h3>
                                </div>
                            </div>

                        </div><!-- /.col-lg-4 -->
                    </div> 
                </div> 
            </section>
            <section id="page_name" class="section_sidepadding mb-4 d-none d-md-block">
                <div class="container">
                    <span class="page_name_text">You are here: </span><?php the_title(); ?>
                </div>
            </section>
<?php 
 endwhile;
get_footer(); 
?>
