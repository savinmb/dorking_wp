<?php
/**
 * The template for displaying search results pages
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>
<section id="video-section" class="" >
    <div class="video-section-div">
    <div class="container-fluid pl-0 pr-0">
        <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/map_landing/map_landing_banner.png" class="w-100">
    </div>
    <div class="overlay_img_txt"><p class="overlay_img_txt_p font_heavy">Search Results</p></div>
    </div>
</section>
<section  class="breadcrumb_wrapper" >
                <div class="container my-3">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo esc_url( home_url( '/' ) ); ?>">Home</a></li>
                            <li class="breadcrumb-item active" >Search</li>
                        </ol>
                    </nav>
                </div>
</section>
<?php if ( have_posts() ) : ?>
<section id="search_options_wrapper" class="description min_height_mapland">
    <div class="container">
        <h1 class="text-center main_search_results_size"><?php printf( __( 'Search Results for: %s', 'twentysixteen' ), '<span>' . esc_html( get_search_query() ) . '</span>' ); ?></h1>
    </div>
</section>
<section  class="description">
    <div class="container">
			<?php
			// Start the loop.
			while ( have_posts() ) : the_post();

				/**
				 * Run the loop for the search to output the results.
				 * If you want to overload this in a child theme then include a file
				 * called content-search.php and that will be used instead.
				 */
				get_template_part( 'template-parts/content', 'search' );

			// End the loop.
			endwhile;

			// Previous/next page navigation.
			the_posts_pagination( array(
				'prev_text'          => __( 'Previous page', 'twentysixteen' ),
				'next_text'          => __( 'Next page', 'twentysixteen' ),
				'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'twentysixteen' ) . ' </span>',
			) );

		// If no content, include the "No posts found" template.
		else :
			get_template_part( 'template-parts/content', 'none' );

		endif;
		?>

</div>
</section>	

<?php get_footer(); ?>
