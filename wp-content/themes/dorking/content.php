<?php
/**
 * The template part for displaying content
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php the_title(); ?>
	</header><!-- .entry-header -->

	

	<?php twentysixteen_post_thumbnail(); ?>

	<div class="entry-content">
		<?php
			/* translators: %s: Name of current post */
			the_content();
		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<a href="<?php echo get_field('link');  ?>">visite this link</a>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
