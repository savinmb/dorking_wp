<?php
/*
    Template Name:Highstreet
*/
get_header(); ?>
           <section id="highstreet_banner" class="" >         
                <div class="video-section-div" style="position: relative">
                    <div class="container-fluid p-0">
                        <img src="<?php echo get_field( 'slider_image', get_the_ID() ); ?>" class="w-100">
                    </div>
                    <div class="overlay_img_txt"><p class="font20 font_heavy"><?php echo get_field( 'text', get_the_ID() ); ?></p></div>
                </div>
           </section>
            <section  class="description pl-20 pr-20">
                <div class="container my-5">
                 <?php 
                  while(have_posts())  : the_post(); 
                 the_content(); 
                 endwhile;
                 ?>
                </div>
           </section>
            <section id="highstreet_boxes"  class="pl-20 pr-20">
                <div class="container search_blocks_wrapper">
                    <div class="row box_div_mr">
                        <div class="col-12">
                            <ul class="blocks_ul">
							    <?php
								$rows = get_field('tiles',get_the_ID());
								foreach($rows as $row) {
									
								if($row['name']=='') {	
								?>
							    <li><a href="<?php echo $row['link']; ?>">
                                    <div class="category_block <?php echo $row['color'];  ?>">
                                        <img class="category_icon img-fluid" src="<?php echo $row['image']; ?>" />
                                    </div></a>
                                </li>
								<?php }
								else if($row['name']!='' && $row['image']=='') { ?>
                                <li><a href="<?php echo $row['link']; ?>">
                                    <div class="category_block <?php echo $row['color'];  ?>">
                                        <div class="category_name"><p><?php echo $row['name'];  ?></p></div> 
                                    </div>
                                </li></a>
								<?php } else { ?>
                                <li><a href="<?php echo $row['link']; ?>">
                                    <div class="category_block" style="background-image:url('<?php echo $row['image'];    ?>');"> 
                                        <div class="category_name block_bgimg"><p><?php echo $row['name'];  ?></p></div> 
                                    </div>
                                </li></a>
								<?php } ?>
								<?php } ?>
								
                               
                            </ul>
                        </div>
                    </div>
                </div>
           </section>
            
            <section id="transport_content" class="mt-4 pr-20 pl-20">
                <div class="container pad0">
                    <div class=" paddingbtm10 margintop25 mb-1">
                        <h1 class="text-center color4a transport_content_head">Getting Here</h1>
                    </div>                   
                    <div class="row mt-4 transport-row">
                        <div class="col-sm-12 col-md-4 col-lg-4  mb-4">
                            <div class="transport_columns road_column">
                                <div class="by_road_img">
                                    <img class="img-fluid transport-columns-w-100"  src="<?php echo get_field( 'image1', get_the_ID() ); ?>" alt="Transport">
                                </div>                            
                                <div class=" text-center mt-4">
                                    <h3 class="transport_below_heading"><?php echo get_field( 'text1', get_the_ID() ); ?></h3>
                                </div>
                            </div>    

                        </div><!-- /.col-lg-4 -->
                        <div class="col-sm-12 col-md-4 col-lg-4  mb-4 public_transport_column">
                            <div class=" transport_columns public_transport_column">
                                <div class="by_public_transport_img">
                                    <img class="img-fluid transport-columns-w-100" src="<?php echo get_field( 'image2', get_the_ID() ); ?>" alt="Trail">
                                </div>
                                <div class="text-center mt-4">
                                    <h3 class="transport_below_heading"><?php echo get_field( 'text2', get_the_ID() ); ?></h3>
                                </div>
                            </div>

                        </div><!-- /.col-lg-4 -->
                        <div class="col-sm-12 col-md-4 col-lg-4  mb-4 footpath_column ">
                            <div class="transport_columns footpath_column">
                                <div class="by_footpath_img">
                                     <img class="img-fluid transport-columns-w-100 px-3" src="<?php echo get_field( 'image3', get_the_ID() ); ?>" alt="foot" >
                                </div>
                                <div class="  text-center mt-4">
                                    <h3 class="transport_below_heading"><?php echo get_field( 'text3', get_the_ID() ); ?></h3>
                                </div>
                            </div>

                        </div><!-- /.col-lg-4 -->
                    </div>
                    <div class="">
                        <h1 class="text-center color4a planvisit_font_size">Plan Your Visit</h1>
        </div>    
                </div> 
            </section>
            
       
            <section id="map_wrapper" class="pl-20 pr-20">
                <div class="container my-5 px-4">
                    <div class="row">
                        <?php include 'svgfile.php';   ?>
                    </div>
                </div>
            </section>
<?php get_footer(); ?>

