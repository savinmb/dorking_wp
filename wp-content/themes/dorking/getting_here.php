<?php

/*
    Template Name:Getting here
*/
get_header(); ?>
<?php 
    while(have_posts())  : the_post();
?>    
     <section id="loyalfree_banner" class="" >
               <div class="video-section-div">
                   <div class="container-fluid pl-0 pr-0">
                   <img src="<?php echo get_field( 'slider_image', get_the_ID() ); ?>" class="w-100">
               </div>
               <div class="overlay_img_txt_getting"><p class="overlay_img_txt_getting_p font_heavy"><?php echo get_field( 'text', get_the_ID() ); ?></p></div>
               </div>
               
           </section>
            <section  class="breadcrumb_wrapper" >
                <div class="container my-3">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                          <li class="breadcrumb-item"><a href="<?php echo site_url(); ?>">Home</a></li>
                          <li class="breadcrumb-item active" ><?php the_title();  ?></li>
                        </ol>
                    </nav>
                </div>
           </section>
            <section  class="description mb-4 pl-20 pr-20">
                <div class="container">
                    <?php the_content(); ?>                    
                </div>
           </section>
            <section id="page_name" class="section_sidepadding mb-4 d-none d-md-block">
                <div class="container">
                    <span class="page_name_text">You are here: </span><?php the_title();  ?>
                </div>
            </section>
<?php
 endwhile;
?>


<?php get_footer(); ?>

