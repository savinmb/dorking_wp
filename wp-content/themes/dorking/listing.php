<?php
/*
    Template Name:Listing
*/
get_header(); 
$post_type=$_GET['type'];
?>
<section id="video-section" class="">
<div class="video-section-div" <?php if(get_field('slider_image',get_the_ID())=='') {  ?>style="position: relative;height:500px;" <?php } ?>>
                    <div class="container-fluid pl-0 pr-0">
                        <img src="<?php echo get_field('slider_image',get_the_ID()) ?>" class="w-100">
                    </div>
                    <div class="overlay_img_txt"><p class="overlay_img_txt_p font_heavy"><?php echo get_field( 'text', get_the_ID() ); ?></p></div>
                </div>
</section>
<?php
$count=1;
$posts = new WP_Query(array('post_type' => $post_type));
if($posts->post_count>0)  { ?>
<section  class="description section-padding d-none d-md-block ">
   <div class="container">
   <div class=""><p>Shops</p></div>
<?php
$j=1;
while ( $posts->have_posts() ) : $posts->the_post();
if(($count%3)==1) {
		echo '<div class="row common-row1 shopclass" id="shoprowcount"'.$j.'>';	
		}	
		?>
		<div class="col-12 col-sm-12 col-md-4 col-lg-4 grow cursor-pointer">
                <div class="magic-box-height bkash-color">
                    <div class="image_container image_container_pad0">
                                    <img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($posts->ID), 'thumbnail' ); ?>" class="img-center img-fluid img_height">
                    </div>
                    <div class=" bklightblue text-center box-head-padding word-wrap">
                        <a href="<?php echo get_permalink();  ?>"><p class="col  color-white magic-box-head-size"><?php the_title();  ?></p></a>
                    </div>
                </div>
        </div>
        <?php
		if (($count % 3) == 0) {
			++$j;
            echo '</div>';
        }
++$count;

endwhile;
?>
	</div>
</section>
<?php  } ?>
<?php
if(get_field( 'required', get_the_ID())=='yes') { ?>
<section id="magic-boxes" class="section-padding d-none d-md-block">
                <div class="container">
				    <?php 
					$i=1;
                    $j=1;
					$rows = get_field('tiles',212);
					
                    foreach($rows as $row) {
					if(($i%3) == 1) {
                                  echo '<div class="row common-row'.$j.'" >';
                    }
					if($row['image'] == ''){
                        ?>
                            
                            <div class="col-12 col-sm-12 col-md-4 col-lg-4">
							    <a href="<?php echo $row['link']; ?>">
                                <div class="magic-box-height <?php echo $row['color'];  ?>">
                                    <div class="text-center magic-box-text-middle word-wrap">
                                        <p><?php echo $row['name']; ?></p>
                                    </div>
                                </div>
								</a>
                            </div>
                             <?php } else { ?>
                            <div class="col-12 col-sm-12 col-md-4 col-lg-4">
							    <a href="<?php echo $row['link']; ?>">
                                <div class=" magic-box-height <?php echo $row['color'];  ?>">
                                    <div class="image_container">
                                        <img src="<?php echo $row['image']; ?>" class="img-center img-fluid">
                                    </div>
                                    <div class=" bk-orange-common text-center box-head-padding">
                                        <p class="col color-white magic-box-head-size"> <?php echo $row['name']; ?> </p>
                                    </div>
                                </div>
								</a>
                            </div>
                <?php } 
				if (($i % 3) == 0 || sizeof($rows)==$i) {
					echo '</div>';
					$j++;
					}
				$i++;
				
				
				}  ?>
                </div> 
            </section>
			<section id="magic-boxes" class="section-padding-carousel d-block d-md-none"> 
                <div id="myCarousel" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
					<?php for($i=0;$i<sizeof($rows);$i++) {  ?>
                        <li data-target="#myCarousel" data-slide-to="<?php echo $i; ?>" <?php if($i==0) {  ?>class="active" <?php } ?>></li>
                    <?php } ?>	
                    </ol>

                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">
					    <?php 
						$c=0;
						foreach($rows as $row) {
						if($row['image'] == ''){
                        ?>
                        <div class="carousel-item <?php if($c==0) {  echo 'active';  }  ?>">
                            <div class="col-12 col-sm-12 col-md-4 col-lg-4 ">
							     <a href="<?php echo $row['link']; ?>">
                                <div class="magic-box-height <?php echo $row['color'];  ?>">
                                    <div class="text-center text-middle magic-box-text-middle word-wrap font14">
                                        <p><?php echo $row['name']; ?></p>
                                    </div>
                                </div>
								</a>
                            </div>
                        </div>
						<?php } else { ?>
                        <div class="carousel-item <?php if($c==0) {  echo 'active';  }  ?>">
                            <div class="col-12 col-sm-12 col-md-4 col-lg-4 ">
							<a href="<?php echo $row['link']; ?>">
                                <div class=" magic-box-height <?php echo $row['color'];  ?>">
                                    <div class="image_container">
                                        <img src="<?php echo $row['image']; ?>" class="img-center img-fluid">
                                    </div>
                                    <div class=" bk-orange-common text-center box-head-padding">
                                        <p class="col color-white font14"> <?php echo $row['name']; ?> </p>
                                    </div>
                                </div>
							</a>	
                            </div>
                        </div>
						<?php 
						          }
						++$c;
						} ?>
                    </div>

                </div>
            </section>





<?php } ?>
<?php get_footer(); ?>
