<?php
/*
  Template Name:Explore
 */
get_header();
?>
<style>
    .cus_atag{
        color: white;
        text-decoration: none;
    }
 a:hover{
        color: white;
        text-decoration: none;
    }    
</style>
<?php
while (have_posts()) : the_post();
?>
<section id="video-section" class="" >
    <div class="video-section-div" <?php if(get_field('slider_image',get_the_ID())=='') {  ?>style="position: relative;height:500px;" <?php } ?>>
        <div class="container-fluid pl-0 pr-0">
            <img src="<?php echo get_field('slider_image',get_the_ID()) ?>" class="w-100">
		</div>
        </div>
        <div class="overlay_img_txt"><p class="overlay_img_txt_p font_heavy"><?php echo get_field( 'text', get_the_ID() ); ?></p></div>
    </div>
</section>
 <section  class="breadcrumb_wrapper" >
                <div class="container my-3">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                          <li class="breadcrumb-item"><a href="<?php echo esc_url( home_url( '/' ) ); ?>">Home</a></li>
                          <li class="breadcrumb-item active" ><?php echo get_the_title();  ?></li>
                        </ol>
                    </nav>
                </div>
</section> 
<section id="search_options_wrapper" class="pl-20 pr-20">
	<div class="container">
        <h1 class="text-center my-3 exploere_head"><?php the_title();  ?></h1>
		<form method="get" action="<?php echo get_permalink(430);  ?>" id="searchform">
        <input class="search_txt_box my-4 search_by_cat" type="text" id="search-term" placeholder="Search by name or type of shop" />
		</form>
          <?php
     $shop_titleOpt = get_field('shop_title_required',get_the_ID());
       if($shop_titleOpt == "yes") {
    ?>
        <p class="ml-5"><?php echo get_field('shop_caption','option');  ?></p>
        <?php } ?>
        </div>
        <div class="container padding0">
            <div class="container padding0">
                <div class="row">
                    <ul class="blocks_ul padding0">
						<?php
                                                if($shop_titleOpt == "yes") {
						$rows = get_field('shop_tiles','option');
						foreach($rows as $row) {
						?>	
						<li>
							<a href="<?php echo $row['link'];  ?>">
							<div class="category_block <?php echo $row['color'];  ?> cursor-pointer">
							<div class="category_name"><p><?php echo $row['name'];  ?></p></div> 
							</div>
							</a>
						</li>
						<?php } 
                                                }
                                                ?>	
					</ul>
                </div>                    
            </div>
        </div>
</section>
<section id="desciption_wrapper" class="pl-20 pr-20">
        <div class="container my-5">
          <?php the_content(); ?>
        </div>
</section>
<section id="magic-boxes" class="section-padding d-none d-md-block mb-5">
                <div class="container">
				    <?php 
					$i=1;
                    $j=1;
					$rows = get_field('tiles',get_the_ID());
                    foreach($rows as $row) {
					if(($i%3) == 1) {
                                  echo '<div class="row common-row'.$j.'" >';
                    }
					if($row['image'] == ''){
                        ?>
                            
                            <div class="col-12 col-sm-12 col-md-4 col-lg-4">
							    <a href="<?php echo $row['link']; ?>">
                                <div class="magic-box-height <?php echo $row['color'];  ?>">
                                    <div class="text-center magic-box-text-middle word-wrap">
                                        <p><?php echo $row['name']; ?></p>
                                    </div>
                                </div>
								</a>
                            </div>
                             <?php } else { ?>
                            <div class="col-12 col-sm-12 col-md-4 col-lg-4">
							    <a href="<?php echo $row['link']; ?>">
                                <div class=" magic-box-height <?php echo $row['color'];  ?>">
                                    <div class="image_container">
                                        <img src="<?php echo $row['image']; ?>" class="img-center img-fluid">
                                    </div>
                                    <div class=" bk-orange-common text-center box-head-padding">
                                        <p class="col color-white magic-box-head-size"> <?php echo $row['name']; ?> </p>
                                    </div>
                                </div>
								</a>
                            </div>
                <?php } 
				if (($i % 3) == 0) {
					echo '</div>';
					$j++;
					}
				$i++;
				
				
				}  ?>
                </div> 
            </section>
			<section id="magic-boxes" class="section-padding-carousel d-block d-md-none mb-5"> 
                <div id="myCarousel" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
					<?php for($i=0;$i<sizeof($rows);$i++) {  ?>
                        <li data-target="#myCarousel" data-slide-to="<?php echo $i; ?>" <?php if($i==0) {  ?>class="active" <?php } ?>></li>
                    <?php } ?>	
                    </ol>

                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">
					    <?php 
						$c=0;
						foreach($rows as $row) {
						if($row['image'] == ''){
                        ?>
                        <div class="carousel-item <?php if($c==0) {  echo 'active';  }  ?>">
                            <div class="col-12 col-sm-12 col-md-4 col-lg-4 ">
							     <a href="<?php echo $row['link']; ?>">
                                <div class="magic-box-height <?php echo $row['color'];  ?>">
                                    <div class="text-center text-middle magic-box-text-middle word-wrap font14">
                                        <p><?php echo $row['name']; ?></p>
                                    </div>
                                </div>
								</a>
                            </div>
                        </div>
						<?php } else { ?>
                        <div class="carousel-item <?php if($c==0) {  echo 'active';  }  ?>">
                            <div class="col-12 col-sm-12 col-md-4 col-lg-4 ">
							<a href="<?php echo $row['link']; ?>">
                                <div class=" magic-box-height <?php echo $row['color'];  ?>">
                                    <div class="image_container">
                                        <img src="<?php echo $row['image']; ?>" class="img-center img-fluid">
                                    </div>
                                    <div class=" bk-orange-common text-center box-head-padding">
                                        <p class="col color-white font14"> <?php echo $row['name']; ?> </p>
                                    </div>
                                </div>
							</a>	
                            </div>
                        </div>
						<?php 
						          }
						++$c;
						} ?>
                    </div>

                </div>
            </section>
<section id="page_name" class="section_sidepadding mb-4 d-none d-md-block">
        <div class="container">
            <span class="page_name_text">You are here: </span><?php the_title();  ?>
        </div>
    </section>
<?php
 endwhile;
?>
<?php get_footer(); ?>
