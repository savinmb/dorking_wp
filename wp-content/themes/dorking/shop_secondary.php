
 <section id="video-section" class="" >
               <div class="container-fluid pl-0 pr-0">
                   <img src="<?php echo get_the_post_thumbnail_url(get_the_ID(),'full'); ?>" class="w-100">
               </div>
           </section>
<?php
 $postType = get_post_type_object(get_post_type(get_the_ID()));
 //print_r($postType);
?>
            <section  class="breadcrumb_wrapper" >
                <div class="container my-3">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb" style="margin-bottom: 0px;">
                          <li class="breadcrumb-item"><a href="<?php echo site_url();  ?>">Home</a></li>
                          <li class="breadcrumb-item" ><a href="<?php echo site_url();  ?>/category/<?php echo $postType->name; ?>"><?php 
                        
                          echo esc_html($postType->labels->singular_name);
                          ?></a></li>
                          <li class="breadcrumb-item active" ><?php the_title(); ?></li>                          
                        </ol>
                        <?php                          
                            if($postType->name == 'blog') {
                        ?> 
                        <div style=" text-align:  right;"><span><?php echo get_the_date( $format, get_the_ID() ); ?></span></div>
                        
                        <?php } ?>
						
						
						
                    </nav>
                   
                </div>
           </section>
            <section id="shop_description_wrapper" class="description">
                <div class="container">
                  <?php the_content(); ?> 
				   <div class="links_wrapper pl-0">
					<ul class="list-unstyled header_social_links d-none d-md-block" style="float:left;">
						<?php if(get_field('trip_advisor',get_the_ID())=='') {  ?>
						<li><a href="<?php echo get_field('trip_advisor',get_the_ID()) ?>" target="_blank">	<img class="img-fluid" src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/social_4.png"/></a></li>
						<?php } ?>
						<?php if(get_field('instagram',get_the_ID())=='') {  ?>
						<li><a href="<?php echo get_field('instagram',get_the_ID()) ?>" target="_blank"><img class="img-fluid" src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/social_1.png"/></a></li>
						<?php } ?>
						<?php if(get_field('facebook',get_the_ID())=='') {  ?>
						<li><a href="<?php echo get_field('facebook',get_the_ID()) ?>" target="_blank"><img class="img-fluid" src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/social_2.png"/></a></li>
						<?php } ?>
						<?php if(get_field('twitter',get_the_ID())=='') {  ?>
						<li><a href="<?php echo get_field('twitter',get_the_ID()) ?>" target="_blank"><img class="img-fluid" src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/social_3.png"/></a></li>
						<?php } ?>
					</ul>
				    </div>
                </div>
				
           </section>
            <?php 
            $gallery_results = get_field('gallery_acf');           
                                       
            ?>
            <section>
                <div class="container">
                    <?php
                       if($gallery_results)
            {  
                    ?>
                    <p>Photos of <?php the_title() ?></p>
                    <div class="row">
                        <?php
                        foreach ($gallery_results as $gallery_result){                            
                        ?>
                        <div class="col-12 col-md-4 col-lg-4 col-sm-6">
                           <img src="<?php echo $gallery_result['url']; ?>" class="img-fluid img-size"/> 
                           <p></p>
                        </div> 
                        <?php } ?>
                    </div>
                      <?php                 
                } 
            ?>
                   <?php $terms = get_the_terms( get_the_ID(), 'post_tag' ); 
			            $draught_links = array();
                                    if($terms){
                                        echo '<p class="my-4">Tags:';
                        foreach ( $terms as $term ) {
                            $draught_links[] = $term->name;
                        }
                                    }
                        $on_draught = join( ", ", $draught_links );
                        echo $on_draught.'</p>';
                     ?>
                     <p class="mb-4">YOU ARE HERE: <?php 
                        $postType = get_post_type_object(get_post_type(get_the_ID()));  
                          echo esc_html($postType->labels->singular_name);
                          ?> / <?php the_title() ?></p>
                </div>
            </section>
          






