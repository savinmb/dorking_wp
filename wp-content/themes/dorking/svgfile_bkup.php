 <div class="container">
                    <div class="w-100 box_style" >  
<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 930 530" preserveAspectRatio="xMidYMid meet" >
<defs id="svgEditorDefs">
<polygon id="svgEditorShapeDefs" />
</defs><rect id="svgEditorBackground" x="0" y="0" width="930" height="530" style="fill: none; stroke: none;"/>
<g id="e23_group"/>
<a href="<?php echo get_permalink(397);  ?>?street=pump-corner"><circle id="e21_circle" cx="456" cy="303" r="13.3154" /></a>
<text x="413" y="42" id="e22_texte" >
<tspan x="413"></tspan><tspan x="436" dy="1.25em" y="" style="font-family:Lucida Sans;">High Street</tspan>
</text>
<a href="<?php echo get_permalink(397);  ?>?street=south-street"><path d="M-2.1247375782075246,-2.465887178297218l-2.4729428221654968,8.239994057231826h6.756699174736099Z"  id="e26_shape" transform="matrix(14.1222 18.8416 -37.0641 6.04497 421.028 379.975)"/></a>
<a href="<?php echo get_permalink(397);  ?>?street=west-street"><path d="M-1.3151866230361264,-5.499743634670242l-5.7781138751369205,5.091284087579847h7.734276022091927Z"  id="e30_shape" transform="matrix(-16.9135 -26.8936 26.8936 -16.9135 279.629 109.395)"/></a>
<a href="<?php echo get_permalink(397);  ?>?street=high-street"><path d="M0.3350032783806714,-6.053732891223747l-0.16130367962233955,10.055618042186474h4.61280685743474Z"  id="e31_shape" transform="matrix(-21.5264 -22.0665 39.0306 -14.8011 639.74 196.222)"/></a>
<text x="192" y="73" id="e32_texte" >West Street</text>
<text style="fill:#4A4A4A;font-family:Lucida Sans;font-size:24px;" x="479" y="309" id="e33_texte" >Pump Corner</text>
<text style="fill:#4A4A4A;font-family:Lucida Sans;font-size:24px;" x="342" y="453" id="e34_texte" >South Street</text>
</svg>
<style>
    #e26_shape{fill:#fff; stroke:#f19101; vector-effect:non-scaling-stroke;stroke-width:3px;}
    #e26_shape:hover{fill:#f19101; cursor: pointer;}
    #e30_shape{fill:#fff; stroke:#3fb4a0; vector-effect:non-scaling-stroke;stroke-width:3px;}
    #e30_shape:hover{fill:#3fb4a0; cursor: pointer;} 
    #e31_shape{fill:#fff; stroke:#ffd303; vector-effect:non-scaling-stroke;stroke-width:3px;}
    #e31_shape:hover{fill:#ffd303; cursor: pointer;} 
    #e21_circle{fill:#fff;stroke:#DC9822;stroke-width:1px;}
    #e21_circle:hover{fill:#DC9822;stroke:#ddd;stroke-width:2px;cursor: pointer}
    .box_style{border: 1px solid #908f8f;box-shadow: 0px 0px 1px #757575;}
    #e32_texte{fill:#4A4A4A;font-family:Lucida Sans;font-size:24px;}
    #e33_texte{fill:#4A4A4A; font-family: Arial; font-size:24.1px;}
    #svgEditorShapeDefs{fill:khaki;stroke:black;vector-effect:non-scaling-stroke;stroke-width:1px;}
	 #e22_texte{fill:#4A4A4A;font-family:Lucida Sans;font-size:24px;}
</style>
<?php
if(@$street!='')
{	
		if($street=='high-street') 
				{ ?>
		<style>
		 #e31_shape {fill:#ffd303; cursor: pointer;} 
		</style>	
		<?php	}
		else if($street=='west-street')
				{ ?>
		<style>
		 #e30_shape {fill:#3fb4a0; cursor: pointer;} 
		</style>	
		<?php	}
		else if($street=='south-street')
				{ ?>
		<style>
		 #e26_shape {fill:#f19101; cursor: pointer;}
		</style>	
		<?php	}
		else
				{ ?>
		<style>
		 #e21_circle {fill:#DC9822;stroke:#ddd;stroke-width:2px;cursor: pointer}
		</style>	
		<?php	
		}
}
?>
</div> 
</div> 