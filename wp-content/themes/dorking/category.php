<?php
/**
* A Simple Category Template
*/
 
get_header(); 
$category = get_queried_object();
?> 
<section id="loyalfree_banner">
    <div class="video-section-div">
		<div class="container-fluid pl-0 pr-0">
		    <?php if(get_field('category_image',$category )=='') {  ?>
			<img src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/map_landing/map_landing_banner.png" class="w-100">
			<?php } else { ?>
			<img src="<?php echo get_field('category_image',$category );  ?>" class="w-100">
			<?php } ?>
		</div>
		<div class="overlay_img_txt_loyal"><p class="overlay_img_txt_loyal_p font20 font_heavy"></p></div>
    </div>             

</section>
<section  class="breadcrumb_wrapper" >
                <div class="container my-3">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                          <li class="breadcrumb-item"><a href="<?php echo esc_url( home_url( '/' ) ); ?>">Home</a></li>
                          <li class="breadcrumb-item active" ><?php echo $category->name;  ?></li>
                        </ol>
                    </nav>
                </div>
</section>
<section id="search_options_wrapper" class="pl-35 pr-35">
    <div class="container map_img_wrapper pad0">
        <div class="mt-1 pad0">
        <?php include 'svgfile.php';   ?>
        </div>
    </div>
</section>
<section id="desciption_wrapper" class="min_height_mapland description pl-35 pr-35 mtop_main" style="padding-top: 30px;">
    <div class="container mtop3rem">
        <div class="mr-left-n15 pleft15">
		    <form method="get" action="<?php echo get_permalink(430);  ?>" id="searchform">
           <input class="search_txt_box ml-0 col-12 col-sm-12 col-md-6" type="text" name="search" id="search-term" placeholder="Search by name or type of shop" />
			</form>			
        </div>
    </div>    
</section>

<?php 
$feat=array();
$loop = new WP_Query( array ('post_type' => array( 'shop', 'events', 'hotel'),'cat' => $category->term_id,'posts_per_page'    => 3,'meta_key' => 'featured','meta_value' => 'yes'));
if($loop->have_posts()) 
	{

	?>
	<section id="magic-boxes" class="description section-padding mb-4">
        <div class="container">
            <div class=""><p><?php echo get_field('featured_title',397);  ?></p></div>
			<div class="row common-row1" id="row1-boxes">
      <?php			
      while($loop->have_posts()) : $loop->the_post();
	  $feat[]=$post->ID;	
      ?>
	    <div class="col-12 col-sm-12 col-md-4 col-lg-4 grow cursor-pointer">
            <div class="magic-box-height">
                <div class="image_container image_container_pad0">
                <img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID), 'thumbnail' ); ?>" class="img-center img-fluid img_height">
                </div>
                <div class=" bk-orange-common text-center box-head-padding word-wrap">
                    <p class="col  color-white magic-box-head-size"> <?php the_title();  ?> </p>
                </div>
            </div>
        </div>
	  <?php	
	  endwhile; ?>
			</div>
	    </div> 
    </section>	
	<?php } 
	wp_reset_query();

	?>
<?php
			$loop = new WP_Query( array ('post_type' => array( 'shop', 'events', 'hotel'),'cat' => $category->term_id,'post__not_in'=>$feat,'posts_per_page' => 9,'paged'=>1));
			
			if($loop->have_posts()) 
				{ ?>
			
			<section id="magic-boxes" class="description section-padding mb-4">
				<div class="container">
					<div class=""><p><?php echo get_field('view_all_section',397);  ?></p></div>
					<form method="post" id="category_form">
						<input type="hidden" name="feat" id="feat" value="<?php echo implode('|',$feat);   ?>">
						<input type="hidden" name="fetch_url" id="fetch_url" value="<?php echo site_url().'/load_more.php';  ?>">
						<input type="hidden" name="form_type" id="form_type" value="category">
						<input type="hidden" name="page_value" id="page_value" value="1">
						<input type="hidden" name="count_value" id="count_value">
						<input type="hidden" id="category_id" name="category_id" value="<?php echo $category->term_id;   ?>">
					</form>
					<div class="over-container">

			<?php
				$i=1;
				while($loop->have_posts()) : $loop->the_post();
		        if(($i%3) == 1) {
                         echo '<div class="row common-row1 level-list" >';
                    }		
			?>
			   
				<?php
				if(($i%9) == 0 && $loop->max_num_pages>1) { ?>
				<div class="col-12 col-sm-12 col-md-4 col-lg-4 grow cursor-pointer load-section mb-4" id="element_<?php echo $i; ?>" style="display:none;">
                    <div class="magic-box-height bkyellow-color">
                        <div class="image_container image_container_pad0">
                        <img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($loop->ID), 'thumbnail' ); ?>" class="img-center img-fluid img_height">
                        </div>
                        <div class=" bk-orange-common text-center box-head-padding word-wrap">
                            <p class="col  color-white magic-box-head-size"> <?php the_title();  ?> </p>
                        </div>
                    </div>
                </div>		
				<div class="col-12 col-sm-12 col-md-4 col-lg-4 cursor-pointer mb-4 load-more cld_more" id="loadelement_<?php echo $i; ?>">
                            <div class="magic-box-height bk-gray  view_more">
                                <div class=" text-center magic-box-text-middle box-head-padding word-wrap">
                                    <p class="col  color-white magic-box-head-size"> view more</p>
                                </div>
                                <div class="image_container img_view_more">
                                    <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/index/viewmore.png" class="img-center img-fluid">
                                </div>
                            </div>
                </div>	
					
				<?php }
				else {   ?>
				<div class="col-12 col-sm-12 col-md-4 col-lg-4 grow cursor-pointer load-section  mb-4" id="element_<?php echo $i; ?>">
                    <div class="magic-box-height bkyellow-color">
                        <div class="image_container image_container_pad0">
                        <img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($loop->ID), 'thumbnail' ); ?>" class="img-center img-fluid img_height">
                        </div>
                        <div class=" bk-orange-common text-center box-head-padding word-wrap">
                            <p class="col  color-white magic-box-head-size"> <?php the_title();  ?> </p>
                        </div>
                    </div>
                </div>	
					
					
					
				<?php	}
				?>
            <?php   
              if (($i % 3) == 0  || $loop->post_count==$i) 
				{
				echo '</div>';
				}			
			    ++$i;
				endwhile;  ?>
				</div>
				</div>
			 </section>	
			 <?php	
			 }
			wp_reset_query();
 
			?>
		</div>
	</div>
</div>
</section>
<?php
$loop = new WP_Query( array ('post_type' => array('blog'),'cat' => $category->term_id,'posts_per_page' => 3));
if($loop->have_posts()) 
	{

	?>
	<section id="magic-boxes" class="description section-padding mb-4">
        <div class="container">
            <div class=""><p><?php echo get_field('get_inspired_section',397);  ?></p></div>
			<div class="row common-row1" id="row1-boxes">
      <?php			
      while($loop->have_posts()) : $loop->the_post();
	  $feat[]=$post->ID;	
      ?>
	    <div class="col-12 col-sm-12 col-md-4 col-lg-4 grow cursor-pointer">
            <div class="magic-box-height">
                <div class="image_container image_container_pad0">
                <img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID), 'thumbnail' ); ?>" class="img-center img-fluid img_height">
                </div>
                <div class=" bk-orange-common text-center box-head-padding word-wrap">
                    <p class="col  color-white magic-box-head-size"> <?php the_title();  ?> </p>
                </div>
            </div>
        </div>
	  <?php	
	  endwhile; ?>
			</div>
	    </div> 
    </section>	
	<?php } 
	wp_reset_query();

?>
<section id="search_options_wrapper" class=" description mb-5 pl-20 pr-20">
<div class="container">
    <h1 class="text-center my-3 exploere_head"><?php echo get_field('browse_title',get_the_ID());  ?></h1>
	 
   
    <p class="ml-5"><?php echo get_field('shop_caption','option');  ?></p>
      
</div>
<div class="container padding0">
    <div class="container padding0">
        <div class="row">
        <ul class="blocks_ul padding0">
		    <?php
                   
                  
			$rows = get_field('shop_tiles','option');
			foreach($rows as $row) {
			?>	
			<li>
			    <a href="<?php echo $row['link'];  ?>">
                <div class="category_block <?php echo $row['color'];  ?> cursor-pointer">
                <div class="category_name"><p><?php echo $row['name'];  ?></p></div> 
				</div>
				</a>
            </li>
			<?php } 
                    
                        ?>
		</ul>
            </div>
        </div>
    </div>
</section> 
<section id="page_name" class="section_sidepadding mb-4 d-none d-md-block">
                <div class="container">
                    <span class="page_name_text">You are here: </span><?php echo $category->name;  ?>
                </div>
</section>
<?php get_footer(); ?>