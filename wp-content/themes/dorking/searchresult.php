<?php
/*
    Template Name:Search Result
*/
get_header(); ?>
<?php
$pid=get_the_ID();
while ( have_posts() ) : the_post();
?>
<div class="video-section-div" <?php if(get_field('slider_image',get_the_ID())=='') {  ?>style="position: relative;height:500px;" <?php } ?>>
                    <div class="container-fluid pl-0 pr-0">
                        <img src="<?php echo get_field('slider_image',get_the_ID()) ?>" class="w-100">
                    </div>
                    <div class="overlay_img_txt"><p class="overlay_img_txt_p font_heavy"><?php echo get_field( 'text', get_the_ID() ); ?></p></div>
                </div>
</section>
 <section  class="breadcrumb_wrapper" >
                <div class="container my-3">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo esc_url( home_url( '/' ) ); ?>">Home</a></li>
                            <li class="breadcrumb-item active" ><?php the_title();  ?></li>
                        </ol>
                    </nav>
                </div>
</section>
<section id="search_options_wrapper" class="description min_height_mapland">
    <div class="container">
        <h1 class="text-center main_search_results_size"><?php echo get_field( 'results_title', get_the_ID() ); ?></h1>
    </div>
</section>

<?php 
endwhile;
?>
  
<?php 
$search_terms=$_GET['search'];
if($search_terms!='')
    {
	$catar=array();
if(isset($_GET['cat']))
	{
	$actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
	$catar=getcatlist($actual_link);
	
	?>
	<section class="section-padding serach_filter_section">
		<div class="container">
			<div class="row mr-0 ml-0">
			    <?php for($i=0;$i<sizeof($catar);$i++) { ?>
				<div class="col-4 col-lg-2 col-md-2 col-sm-4 mr-1 mb-1 search_filter_div cat_search" id="catsr_<?php echo $catar[$i];  ?>"><input type="hidden" id="caturl_<?php echo $catar[$i];  ?>" value="cat=<?php echo $catar[$i]; ?>"><span class="search_filter_span"><?php echo get_cat_name($catar[$i]);  ?></span><i class="fa fa-close search_filter_iframe cat_close" id="catcl_<?php echo $catar[$i];  ?>"></i></div>
				<?php }  ?>
			</div>    
		</div>
	</section>
	<?php
	}


$category=getCatresult($search_terms);	
if(sizeof($category)>0)  { ?>
<section id="magic-boxes" class="section-padding">
    <div class="container">
	<?php 
	$i=1;
	foreach($category as $cat) {  
		if(($i%3) == 1) {
        echo '<div class="row common-row2">';
        }
		?>
			<div class="col-12 col-sm-12 col-md-4 col-lg-4 grow cursor-pointer mb-4">
			    <a  class="cat-search <?php if(in_array($cat->term_id,$catar)) {  echo 'disable-div';  }  ?>" id="cat_<?php echo $cat->term_id; ?>" >
                <div class="magic-box-height bkcolor-cyan<?php echo rand(1,3); ?>">
                    <div class="image_container">
                       
                    </div>
                    <div class=" bk-orange-common text-center box-head-padding word-wrap">
                        <p class="  color-white magic-box-head-size"><?php echo get_cat_name($cat->term_id); ?></p>
                    </div>
                </div>
				</a>
            </div>
		<?php
		if (($i % 3) == 0 || sizeof($category)==$i) {
        echo '</div>';
	    }
	   $i++;
	} 
	?>
	</div>
</section>	

<?php }  ?>
<input type="hidden" name="fetch_url" id="fetch_url" value="<?php echo site_url().'/load_more.php';  ?>">
<?php
$count=1;
$posts = new WP_Query(array('post_type' => 'shop','posts_per_page' => 9,'s' =>$search_terms,'category__in' =>$catar));
if($posts->post_count>0)  { ?>
<section  class="description section-padding mb-4">
   <div class="container">
   <div class=""><p>
   
   <?php 
   echo get_field( 'shop_title',$pid); ?></p></div>
   <form method="post" id="shop_form">
			<input type="hidden" name="category" value="<?php echo implode('|',$catar);   ?>">
			<input type="hidden" name="form_type" value="shop">
			<input type="hidden" name="page_value1" id="page_value1" value="1">
			<input type="hidden" name="count_value1" id="count_value1">
			<input type="hidden" name="search_term" value="<?php echo $search_terms;   ?>">
	</form>
   <div class="over-container-shop">
<?php
while ( $posts->have_posts() ) : $posts->the_post();
if(($count%3)==1) {
		echo '<div class="row common-row1 shopclass level-list">';	
		}	
		?>
		<?php
				if(($count%9) == 0 && $posts->max_num_pages>1) { ?>
				<div class="col-12 col-sm-12 col-md-4 col-lg-4 grow cursor-pointer load-section1 mb-4" id="element1_<?php echo $count; ?>" style="display:none;">
                    <div class="magic-box-height bkyellow-color">
                        <div class="image_container image_container_pad0">
                        <img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($posts->ID), 'thumbnail' ); ?>" class="img-center img-fluid img_height">
                        </div>
                        <div class=" bk-orange-common text-center box-head-padding word-wrap">
                            <p class="col  color-white magic-box-head-size"> <?php the_title();  ?> </p>
                        </div>
                    </div>
                </div>		
				<div class="col-12 col-sm-12 col-md-4 col-lg-4 cursor-pointer mb-4 load-more ld_more1" id="loadelement1_<?php echo $count; ?>">
                            <div class="magic-box-height bk-gray view_more">
                                <div class=" text-center magic-box-text-middle box-head-padding word-wrap">
                                    <p class="col  color-white magic-box-head-size"> view more</p>
                                </div>
                                <div class="image_container img_view_more">
                                    <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/index/viewmore.png" class="img-center img-fluid">
                                </div>
                            </div>
                </div>	
					
				<?php }
				else {   ?>
				<div class="col-12 col-sm-12 col-md-4 col-lg-4 grow cursor-pointer load-section1 mb-4" id="element1_<?php echo $count; ?>">
                    <div class="magic-box-height bkyellow-color">
                        <div class="image_container image_container_pad0">
                        <img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($posts->ID), 'thumbnail' ); ?>" class="img-center img-fluid img_height">
                        </div>
                        <div class=" bk-orange-common text-center box-head-padding word-wrap">
                            <p class="col  color-white magic-box-head-size"> <?php the_title();  ?> </p>
                        </div>
                    </div>
                </div>	
				<?php	}
				?>
		<?php
		if (($count % 3) == 0 || $posts->post_count==$count) {
			  echo '</div>';
        }
++$count;

endwhile;
?>
    </div>
	</div>
</section>
<?php  }
$count=1;
$posts = new WP_Query(array('post_type' => 'events','posts_per_page' => 9,'s' =>$search_terms,'category__in' =>$catar));
if($posts->post_count>0)  { ?>
<section  class="description section-padding">
   <div class="container">
   <div class=""><p><?php echo get_field('events_title',$pid); ?></p></div>
    <form method="post" id="event_form">
			<input type="hidden" name="category" value="<?php echo implode('|',$catar);   ?>">
			<input type="hidden" name="form_type" value="event">
			<input type="hidden" name="page_value2" id="page_value2" value="1">
			<input type="hidden" name="count_value2" id="count_value2">
			<input type="hidden" name="search_term" value="<?php echo $search_terms;   ?>">
	</form>
   <div class="over-container-event">
<?php
while ( $posts->have_posts() ) : $posts->the_post();
if(($count%3)==1) {
		echo '<div class="row common-row1 eventclass level-list">';	
		}	
		?>
		<?php
				if(($count%9) == 0 && $posts->max_num_pages>1) { ?>
				<div class="col-12 col-sm-12 col-md-4 col-lg-4 grow cursor-pointer load-section2 mb-4" id="element2_<?php echo $count; ?>" style="display:none;">
                    <div class="magic-box-height bkyellow-color">
                        <div class="image_container image_container_pad0">
                        <img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($posts->ID), 'thumbnail' ); ?>" class="img-center img-fluid img_height">
                        </div>
                        <div class=" bk-orange-common text-center box-head-padding word-wrap">
                            <p class="col  color-white magic-box-head-size"> <?php the_title();  ?> </p>
                        </div>
                    </div>
                </div>		
				<div class="col-12 col-sm-12 col-md-4 col-lg-4 cursor-pointer mb-4 load-more ld_more2" id="loadelement2_<?php echo $count; ?>">
                            <div class="magic-box-height bk-gray view_more">
                                <div class=" text-center magic-box-text-middle box-head-padding word-wrap">
                                    <p class="col  color-white magic-box-head-size"> view more</p>
                                </div>
                                <div class="image_container img_view_more">
                                    <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/index/viewmore.png" class="img-center img-fluid">
                                </div>
                            </div>
                </div>	
					
				<?php }
				else {   ?>
				<div class="col-12 col-sm-12 col-md-4 col-lg-4 grow cursor-pointer  mb-4 load-section2" id="element2_<?php echo $count; ?>">
                    <div class="magic-box-height bkyellow-color">
                        <div class="image_container image_container_pad0">
                        <img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($posts->ID), 'thumbnail' ); ?>" class="img-center img-fluid img_height">
                        </div>
                        <div class=" bk-orange-common text-center box-head-padding word-wrap">
                            <p class="col  color-white magic-box-head-size"> <?php the_title();  ?> </p>
                        </div>
                    </div>
                </div>	
				<?php	}
				?>
        <?php
		if (($count % 3) == 0  || $posts->post_count==$count) {
		     echo '</div>';
        }
++$count;

endwhile;
?> 
    </div>
	</div>
</section>
<?php  }
$count=1;
$posts = new WP_Query(array('post_type' => 'hotel','posts_per_page' => 9,'s' =>$search_terms,'category__in' =>$catar));
if($posts->post_count>0)  { ?>
<section  class="description section-padding">
   <div class="container">
   <div class=""><p><?php echo get_field( 'hotel_title',$pid); ?></p></div>
   <form method="post" id="hotel_form">
			<input type="hidden" name="category" value="<?php echo implode('|',$catar);   ?>">
			<input type="hidden" name="form_type" value="hotel">
			<input type="hidden" name="page_value3" id="page_value3" value="1">
			<input type="hidden" name="count_value3" id="count_value3">
			<input type="hidden" name="search_term" value="<?php echo $search_terms;   ?>">
	</form>
   <div class="over-container-hotel">
<?php
while ( $posts->have_posts() ) : $posts->the_post();
if(($count%3)==1) {
		echo '<div class="row common-row1 hotelclass level-list">';	
		}	
		?>
		<?php
				if(($count%9) == 0 && $posts->max_num_pages>1) { ?>
				<div class="col-12 col-sm-12 col-md-4 col-lg-4 grow cursor-pointer load-section3 mb-4" id="element3_<?php echo $count; ?>" style="display:none;">
                    <div class="magic-box-height bkyellow-color">
                        <div class="image_container image_container_pad0">
                        <img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($posts->ID), 'thumbnail' ); ?>" class="img-center img-fluid img_height">
                        </div>
                        <div class=" bk-orange-common text-center box-head-padding word-wrap">
                            <p class="col  color-white magic-box-head-size"> <?php the_title();  ?> </p>
                        </div>
                    </div>
                </div>		
				<div class="col-12 col-sm-12 col-md-4 col-lg-4 cursor-pointer  mb-4 load-more ld_more3" id="loadelement3_<?php echo $count; ?>">
                            <div class="magic-box-height bk-gray view_more">
                                <div class=" text-center magic-box-text-middle box-head-padding word-wrap">
                                    <p class="col  color-white magic-box-head-size"> view more</p>
                                </div>
                                <div class="image_container img_view_more">
                                    <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/index/viewmore.png" class="img-center img-fluid">
                                </div>
                            </div>
                </div>	
					
				<?php }
				else {   ?>
				<div class="col-12 col-sm-12 col-md-4 col-lg-4 grow cursor-pointer  mb-4 load-section3" id="element3_<?php echo $count; ?>">
                    <div class="magic-box-height bkyellow-color">
                        <div class="image_container image_container_pad0">
                        <img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($posts->ID), 'thumbnail' ); ?>" class="img-center img-fluid img_height">
                        </div>
                        <div class=" bk-orange-common text-center box-head-padding word-wrap">
                            <p class="col  color-white magic-box-head-size"> <?php the_title();  ?> </p>
                        </div>
                    </div>
                </div>	
				<?php	}
				?>
        <?php
		if (($count % 3) == 0  || $posts->post_count==$count) {
		    echo '</div>';
        }
++$count;

endwhile;
?>
	</div>
	</div>
</section>
<?php  }

}
?>
  
<?php get_footer(); ?>
