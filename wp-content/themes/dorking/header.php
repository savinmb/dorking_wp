<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Rescue Air 1.0
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
    <head>
        <meta charset="<?php bloginfo('charset'); ?>">
        <meta name="viewport" content="width=device-width">
        <link rel="shortcut icon" href="<?php bloginfo('stylesheet_directory'); ?>/images/favicon.ico" type="image/x-icon">
        <link rel="icon" href="<?php bloginfo('stylesheet_directory'); ?>/images/favicon.png" type="image/x-icon">
        <link rel="profile" href="http://gmpg.org/xfn/11">
        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
        <!--[if lt IE 9]>
                <script src="<?php echo esc_url(get_template_directory_uri()); ?>/js/html5.js"></script>
                <![endif]-->
        <script src="<?php echo esc_url(get_template_directory_uri()); ?>/js/jquery-3.3.1.min.js"></script>
        <?php wp_head(); ?>
    </head>

    <body <?php body_class(); ?>>
        <header class="header">
            <div class="container" >
                <div class="row header_top_section">
                    <div class="col-5 col-lg-6 dorking_log d-none d-md-block">
                        <a href="<?php echo esc_url(home_url('/')); ?>"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/index/Dorking_Secondary_HCS_RGB.png" class=""></a>
                    </div>                    
                    <div class="col-7 col-lg-6 pad0">                    
                       <ul class="list-unstyled header_social_links d-none d-md-block">
                                                <li><a class="escapetodork" href="<?php echo esc_url(home_url('/')); ?>" target="_blank">#EscapeToDorking</a> </li>
						<li>  
						<a href="<?php echo get_field('trip_link',212) ?>" target="_blank">
						<img class="img-fluid" src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/social_4.png"/></a> 
						</li>
						<li>  
						<a href="<?php echo get_permalink(49);  ?>" target="_blank">
						<img class="img-fluid" src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/footer_img_1.png"/></a> 
						</li>
						<li>
						<a href="<?php echo get_field('instagram_link',212) ?>" target="_blank">
						<img class="img-fluid" src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/social_1.png"/></a> 
						</li>
						<li>  
						<a href="<?php echo get_field('facebook_link',212) ?>" target="_blank">
						<img class="img-fluid" src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/social_2.png"/></a> 
						</li>
						<li>
						<a href="<?php echo get_field('twitter_link',212) ?>" target="_blank">
						<img class="img-fluid" src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/social_3.png"/></a> 
						</li>
						
						</ul>
                       
							<?php echo get_search_form( $echo );  ?>
                        </div>
                    </div>
                </div>
            </div>
            <!--MEGA MENU START-->
       

            <div class=" d-none d-md-block" id="navbarSupportedContent">
			<div class="logo_sticky"><a href="<?php echo esc_url(home_url('/')); ?>"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/index/sticky_dorkinglogo.png" class="img-fluid grow cursor-pointer" ></a></div>
            <div class="container pad0 d-none d-md-block" >
            <?php
                            wp_nav_menu(array(
                                'theme_location' => 'primary',
                                'menu_class' => 'primary-menu',
                            ));
            ?>
            </div>
                        <div class="close_new" style=""><img src="<?php bloginfo('stylesheet_directory'); ?>/images/index/close_icon_menu.png"/></div>
			<div class="social_sticky">
                        <ul class="list-unstyled header_social_links d-none d-md-block">
                            <li>  <a href="<?php echo get_field('instagram_link',212) ?>" target="_blank"><img class="img-fluid grow cursor-pointer" src="<?php bloginfo('stylesheet_directory'); ?>/images/index/stickyInstagram.png"/></a> </li>
                            <li>  <a href="<?php echo get_field('facebook_link',212) ?>" target="_blank"><img class="img-fluid grow cursor-pointer" src="<?php bloginfo('stylesheet_directory'); ?>/images/index/stickyFacebook1.png"/></a> </li>
                            <li>  <a href="<?php echo get_field('twitter_link',212) ?>" target="_blank"><img class="img-fluid grow cursor-pointer" src="<?php bloginfo('stylesheet_directory'); ?>/images/index/stickyTwitter1.png"/></a> </li>
                           
                        </ul>
                    </div>
            </div>
            
            <div class="col-12 pad0 mob_header">
                 <!--search-->
                 <div class=" d-block d-md-none" id="navbarSupportedContentMob">
                <div class="sm_header_items px-1 pt-2 "style="float: right;margin: 0px;">
                
                    <span id="search_show" class="fa fa-search mr-1 mt-2 search_icon" ></span>
                </div>
                 
                 <!--menu_line-->
                 
				 
                 <div class=" d-block d-md-none" style="float: left;position: absolute;z-index: 111;  width: 100%;" >
                    <?php
                                    wp_nav_menu(array(
                                        'theme_location' => 'primary',
                                        'menu_class' => 'primary-menu',
                                    ));
                    ?>
                </div> 
				
                    
                <!--logo-->
                <div class="d-block d-md-none logo_mob"style="text-align: center;"> 
                        <a href="<?php echo esc_url(home_url('/')); ?>"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/index/logo_sm.png" class=""></a>
                </div>
               
                
                <div id="search_wrapper" class="sidenav d-block d-md-none ">
                <a href="javascript:void(0)" class="closebtn d-block d-md-none" id="search_close">&times;</a>
                <form method="get" action="<?php echo esc_url(home_url('/')); ?>">
                    <div class="input-group search_items_group">
                        <input autofocus type="text" name="s" value="<?php echo get_search_query(); ?>" id="search" class="form-control" placeholder="Search..." aria-label="Search" > 
                        <div class="input-group-append">
                            <button type="submit" class="input-group-text"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                </form>          
            </div>
                </div>
                  
               
            </div>
           
        </header>    
        <main role="main" style="margin-top: 0px;">		