<?php
/*
  Template Name:Bid Login
 */
get_header();
$post_type = 'bid';
if (get_field('slider_image', get_the_ID())) {
    ?>
    <section id="loyalfree_banner">
        <div class="video-section-div">
            <div class="container-fluid pl-0 pr-0">
                <img src="<?php echo get_field('slider_image', get_the_ID()); ?>" class="w-100">
            </div>
            <div class="overlay_img_txt_loyal"><p class="overlay_img_txt_loyal_p font20 font_heavy"></p></div>
        </div>             
    </section>
<?php } ?>
<section  class="breadcrumb_wrapper" >
    <div class="container my-3">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active" ><?php
$postType = get_post_type_object($post_type);
echo esc_html($postType->labels->singular_name);
?></li>
            </ol>
        </nav>
    </div>
</section>
<section  class="description section-padding">
    <div class="container">
       <!--<div class=""><p><?php //echo esc_html($postType->labels->singular_name);    ?></p></div>-->
        <form method="post" id="bid_form">
            <input type="hidden" name="fetch_url" id="fetch_url" value="<?php echo site_url() . '/load_more.php'; ?>">
            <input type="hidden" name="form_type" id="form_type" value="bid">
            <input type="hidden" name="page_value" id="page_value" value="1">
            <input type="hidden" name="count_value" id="count_value">
            <input type="hidden" id="post_type" name="post_type" value="<?php echo $post_type; ?>">
        </form>   
        <div class="over-container">
            <?php
            $fargs = array(
                'post_type' => $post_type,
                'posts_per_page' => 9,
                'paged' => 1
            );
            $loop = new WP_Query($fargs);
            if ($loop->have_posts()) {
                $i = 1;
                while ($loop->have_posts()) : $loop->the_post();
                    if (($i % 3) == 1) {
                        echo '<div class="row common-row1 level-list" >';
                    }
                    ?>

                    <?php if (($i % 9) == 0 && $loop->max_num_pages > 1) { ?>
                        <div class="col-12 col-sm-12 col-md-4 col-lg-4 grow cursor-pointer load-section mb-4" id="element_<?php echo $i; ?>" style="display:none;">
                            <div class="magic-box-height bkyellow-color">
                                <div class="image_container image_container_pad0">
            <?php
            if (has_post_thumbnail()) {
                ?>
                                        <img src="<?php echo wp_get_attachment_url(get_post_thumbnail_id($loop->ID), 'thumbnail'); ?>" class="img-center img-fluid img_height">
                                    <?php } ?>
                                </div>
                                <div class=" bk-orange-common text-center box-head-padding word-wrap">
                                    <p class="col  color-white magic-box-head-size"> <?php the_title(); ?> </p>
                                </div>
                            </div>
                        </div>		
                        <div class="col-12 col-sm-12 col-md-4 col-lg-4 cursor-pointer mb-4 load-more ald_more" id="loadelement_<?php echo $i; ?>">
                            <div class="magic-box-height bk-gray  view_more">
                                <div class=" text-center magic-box-text-middle box-head-padding word-wrap">
                                    <p class="col  color-white magic-box-head-size"> view more</p>
                                </div>
                                <div class="image_container img_view_more">
                                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/index/viewmore.png" class="img-center img-fluid">
                                </div>
                            </div>
                        </div>	

        <?php } else {
            ?>
                        <div class="col-12 col-sm-12 col-md-4 col-lg-4 grow cursor-pointer load-section  mb-4" id="element_<?php echo $i; ?>">
                            <div class="magic-box-height bkyellow-color">
                                <div class="image_container image_container_pad0">
                                     <?php
            if (has_post_thumbnail()) {
                ?>
                                    <img src="<?php echo wp_get_attachment_url(get_post_thumbnail_id($loop->ID), 'thumbnail'); ?>" class="img-center img-fluid img_height">
                                     <?php } ?>
                                </div>
                                <div class=" bk-orange-common text-center box-head-padding word-wrap">
                                    <p class="col  color-white magic-box-head-size"> <?php the_title(); ?> </p>
                                </div>
                            </div>
                        </div>	



        <?php }
        ?>
                    <?php
                    if (($i % 3) == 0 || $loop->post_count == $i) {
                        echo '</div>';
                    }
                    ++$i;
                endwhile;
            }

            wp_reset_query();
            ?>
        </div>
    </div>
</section>


<?php
get_footer();
?>
