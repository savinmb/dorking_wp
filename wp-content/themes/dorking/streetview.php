<?php
/*
    Template Name:Street View
*/
get_header(); ?>
<?php
while ( have_posts() ) : the_post();
$street=$_GET['street'];
?>
<section id="loyalfree_banner">
    <div class="video-section-div" <?php if(get_field('slider_image',get_the_ID())=='') {  ?>style="position: relative;height:500px;" <?php } ?>>
		<div class="container-fluid pl-0 pr-0">
			<img src="<?php echo get_field('slider_image',get_the_ID()) ?>" class="w-100">
		</div>
		<div class="overlay_img_txt_loyal"><p class="overlay_img_txt_loyal_p font20 font_heavy"><?php echo get_field( 'text', get_the_ID() ); ?></p></div>
    </div>             

</section>
<section  class="breadcrumb_wrapper" >
                <div class="container my-3">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                          <li class="breadcrumb-item"><a href="<?php echo esc_url( home_url( '/' ) ); ?>">Home</a></li>
                          <li class="breadcrumb-item active" ><?php echo get_the_title();  ?></li>
                        </ol>
                    </nav>
                </div>
</section>
<?php
endwhile;
?>
<section id="search_options_wrapper" class="pl-35 pr-35">
    <div class="container pad0 text-center">
        <h2 class="my-3 exploere_head"><?php echo get_field('explore_title',get_the_ID());  ?></h2>
    </div>
    <div class="container map_img_wrapper pad0">
        <div class="mt-1">
        <?php include 'svgfile.php';   ?>
        </div>
    </div>
</section>
<section id="desciption_wrapper" class="min_height_mapland description pl-35 pr-35 mtop_main" style="padding-top:35px;">
    <div class="container mtop3rem  ">
        <div class="row">
            <?php echo get_field($street,get_the_ID()) ?>			 
        </div>
        <div class="mr-left-n15 mr-right-n15">
		    <form method="get" action="<?php echo get_permalink(430);  ?>" id="searchform">
           <input class="search_txt_box ml-0 col-12 col-sm-12 col-md-6 bdrstyle_input" name="search" type="text" id="search-term" placeholder="Search by name or type of shop" />
			</form>			
        </div>
    </div>    
</section>

<?php
$feat=array();
$fargs= array(
        'post_type' => array( 'shop', 'events', 'hotel'),
		'posts_per_page'    => 3,
        'meta_key'          => 'featured',
        'meta_value'        => 'yes',
        'tax_query' => array(
            array(
                'taxonomy' => 'Street',
                'field' => 'slug',
                'terms' => $street,
            ),
        ),
     );
$loop = new WP_Query($fargs);
if($loop->have_posts()) 
	{

	?>
	<section id="magic-boxes" class="description section-padding">
        <div class="container">
            <div class=""><p><?php echo get_field('featured_title',get_the_ID());  ?></p></div>
			<div class="row common-row1" id="row1-boxes">
      <?php			
      while($loop->have_posts()) : $loop->the_post();
	  $feat[]=$post->ID;	
      ?>
	    <div class="col-12 col-sm-12 col-md-4 col-lg-4 grow cursor-pointer  mb-4">
            <div class="magic-box-height">
                <div class="image_container image_container_pad0">
                <img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID), 'thumbnail' ); ?>" class="img-center img-fluid img_height">
                </div>
                <div class=" bk-orange-common text-center box-head-padding word-wrap">
                    <p class="col color-white magic-box-head-size color4a"> <?php the_title();  ?> </p>
                </div>
            </div>
        </div>
	  <?php	
	  endwhile; ?>
			</div>
	    </div> 
    </section>	
	<?php } 
	wp_reset_query();

	?>
	<section id="magic-boxes" class="description pl-20 pr-20 mb-4">
        <div class="container"> 
            <div class=""><p class="color4a"><?php echo get_field('view_all_section',get_the_ID());  ?></p></div>
			<form method="post" id="street_form">
			<input type="hidden" name="feat" id="feat" value="<?php echo implode('|',$feat);   ?>">
			<input type="hidden" name="fetch_url" id="fetch_url" value="<?php echo site_url().'/load_more.php';  ?>">
			<input type="hidden" name="form_type" id="form_type" value="street">
			<input type="hidden" name="page_value" id="page_value" value="1">
			<input type="hidden" name="count_value" id="count_value">
			<input type="hidden" id="street_v" name="street_v" value="<?php echo $street;   ?>">
			</form>
			<div class="over-container">
			<?php
			
			
			
			$fargs= array(
				'post__not_in' => $feat,
				'post_type' => array( 'shop', 'events', 'hotel'),
				'posts_per_page' => 9,
				'paged'=>1,
				'tax_query' => array(
					array(
						'taxonomy' => 'Street',
						'field' => 'slug',
						'terms' => $street,
					),
				),
			 );
			$loop = new WP_Query($fargs);
			if($loop->have_posts()) 
				{ 
				$i=1;
				while($loop->have_posts()) : $loop->the_post();
		        if(($i%3) == 1) {
                         echo '<div class="row common-row1 level-list" >';
                    }		
			?>
			   
				<?php
				if(($i%9) == 0 && $loop->max_num_pages>1) { ?>
				<div class="col-12 col-sm-12 col-md-4 col-lg-4 grow cursor-pointer load-section mb-4" id="element_<?php echo $i; ?>" style="display:none;">
                    <div class="magic-box-height bkyellow-color">
                        <div class="image_container image_container_pad0">
                        <img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($loop->ID), 'thumbnail' ); ?>" class="img-center img-fluid img_height">
                        </div>
                        <div class=" bk-orange-common text-center box-head-padding word-wrap">
                            <p class="col  color-white magic-box-head-size"> <?php the_title();  ?> </p>
                        </div>
                    </div>
                </div>		
				<div class="col-12 col-sm-12 col-md-4 col-lg-4 cursor-pointer mb-4 load-more ld_more" id="loadelement_<?php echo $i; ?>">
                            <div class="magic-box-height bk-gray  view_more">
                                <div class=" text-center magic-box-text-middle box-head-padding word-wrap">
                                    <p class="col  color-white magic-box-head-size"> view more</p>
                                </div>
                                <div class="image_container img_view_more">
                                    <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/index/viewmore.png" class="img-center img-fluid">
                                </div>
                            </div>
                </div>	
					
				<?php }
				else {   ?>
				<div class="col-12 col-sm-12 col-md-4 col-lg-4 grow cursor-pointer load-section  mb-4" id="element_<?php echo $i; ?>">
                    <div class="magic-box-height bkyellow-color">
                        <div class="image_container image_container_pad0">
                        <img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($loop->ID), 'thumbnail' ); ?>" class="img-center img-fluid img_height">
                        </div>
                        <div class=" bk-orange-common text-center box-head-padding word-wrap">
                            <p class="col  color-white magic-box-head-size"> <?php the_title();  ?> </p>
                        </div>
                    </div>
                </div>	
					
					
					
				<?php	}
				?>
            <?php   
              if (($i % 3) == 0  || $loop->post_count==$i) 
				{
				echo '</div>';
				}			
			    ++$i;
				endwhile;
			 }
			 
			wp_reset_query();
 
			?>
		</div>
	</div>

</section>
<?php
$fargs= array(
        'post_type' =>'blog',
		'posts_per_page'    => 3,
        'tax_query' => array(
            array(
                'taxonomy' => 'Street',
                'field' => 'slug',
                'terms' => $street,
            ),
        ),
     );
$loop = new WP_Query($fargs);
if($loop->have_posts()) 
	{

	?>
	<section id="magic-boxes" class="description pl-20 pr-20 ">
        <div class="container">
            <div class=""><p><?php echo get_field('get_inspired_section',get_the_ID());  ?></p></div>
			<div class="row common-row1" id="row1-boxes">
      <?php			
      while($loop->have_posts()) : $loop->the_post();
	  $feat[]=$post->ID;	
      ?>
	    <div class="col-12 col-sm-12 col-md-4 col-lg-4 grow cursor-pointer">
            <div class="magic-box-height">
                <div class="image_container image_container_pad0">
                <img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID), 'thumbnail' ); ?>" class="img-center img-fluid img_height">
                </div>
                <div class=" bk-orange-common text-center box-head-padding word-wrap">
                    <p class="col  color-white magic-box-head-size"> <?php the_title();  ?> </p>
                </div>
            </div>
        </div>
	  <?php	
	  endwhile; ?>
			</div>
	    </div> 
    </section>	
	<?php } 
	wp_reset_query();

?>
<section id="search_options_wrapper" class="pl-20 pr-20">
	<div class="container">
        
          <?php
     $shop_titleOpt = get_field('shop_title_required',get_the_ID());
       if($shop_titleOpt == "yes") {
    ?>
        <p class="ml-5"><?php echo get_field('shop_caption','option');  ?></p>
        <?php } ?>
        </div>
        <div class="container padding0">
            <div class="container padding0">
                <div class="row">
                    <ul class="blocks_ul padding0">
						<?php
                                                if($shop_titleOpt == "yes") {
						$rows = get_field('shop_tiles','option');
						foreach($rows as $row) {
						?>	
						<li>
							<a href="<?php echo $row['link'];  ?>">
							<div class="category_block <?php echo $row['color'];  ?> cursor-pointer">
							<div class="category_name"><p><?php echo $row['name'];  ?></p></div> 
							</div>
							</a>
						</li>
						<?php } 
                                                }
                                                ?>	
					</ul>
                </div>                    
            </div>
        </div>
</section>
<?php get_footer(); ?>
